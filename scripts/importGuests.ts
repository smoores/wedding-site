import { readFileSync } from 'fs'
import { getConnection } from '../db'
import { Guest } from '../db/models/Guest'
import { createRsvp } from '../db/queries'

function generateRsvpCode() {
  const letters = 'abcdefg'
  const firstIndex = Math.floor(Math.random() * 7)
  const secondIndex = Math.floor(Math.random() * 7)
  const thirdIndex = Math.floor(Math.random() * 7)
  const fourthIndex = Math.floor(Math.random() * 7)
  return letters[firstIndex] + letters[secondIndex] + letters[thirdIndex] + letters[fourthIndex]
}

async function main() {
  const rsvpsFile = readFileSync('./rsvps.json', { encoding: 'utf8' })
  const rsvps: Pick<Guest, 'name' | 'isPlusOne'>[][] = JSON.parse(rsvpsFile)
  await getConnection()
  for (const rsvpGuests of rsvps) {
    const code = generateRsvpCode()
    await createRsvp(code, rsvpGuests)
  }
}

main()
