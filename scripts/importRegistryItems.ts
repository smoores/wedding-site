import { readFileSync } from 'fs'
import { getConnection } from '../db'
import { RegistryItemDbRecord } from '../db/models/RegistryItem'

async function main() {
  const registryItemsFile = readFileSync('./registry_items.json', { encoding: 'utf8' })
  const registryItems: RegistryItemDbRecord[] = JSON.parse(registryItemsFile)

  const connection = await getConnection()
  connection.query(
    `
    INSERT INTO registry_items
    (
      image_url,
      store_url,
      store_name,
      name,
      price,
      requested_number
    )
    VALUES
      ${registryItems.map((_, startIndex) => {
        const params = Array.from({ length: 6 })
          .fill(undefined)
          .map((_, index) => startIndex * 6 + index + 1)
          .map((index) => `$${index}`)
        return `(${params.join(', ')})`
      }).join(`,
      `)}
  `,
    registryItems.flatMap((item) => [
      item.image_url,
      item.store_url,
      item.store_name,
      item.name,
      item.price,
      item.requested_number,
    ])
  )
}

main()
