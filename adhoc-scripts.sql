-- Everyone who needs the shuttle
-- on the way _to_ the venue (add like 17 for way back)
select
  users.id,
  coalesce(guests.name, answering_for),
  accommodations,
  shuttle,
  brunch
from accomm_and_transport
join users
  on accomm_and_transport.user_id = users.id
  left join guests on guests.rsvp_code = users.rsvp_code
where
  (guests.attending or guests.attending is null)
  and shuttle
  and (
    guests.name is null or
    not guests.name in (
      'Phillip Diffley',
      'Zachary John Beers Post',
      'Taylor Higgins',
      'Reid Allen',
      'Sabrina Shaikh',
      'David Frank III',
      'Eileen Higgins',
      'Sarah Higgins',
      'Brady Moore',
      'Kyra Richards'
    )
  );

-- Everyone who's coming to the brunch
select
  users.id,
  coalesce(guests.name, answering_for),
  accommodations,
  shuttle,
  brunch
from accomm_and_transport
join users
  on accomm_and_transport.user_id = users.id
  left join guests on guests.rsvp_code = users.rsvp_code
where
  (guests.attending or guests.attending is null)
  and brunch
