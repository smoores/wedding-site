import { useRouter } from 'next/router'
import { useState } from 'react'
import { AccommAndTransportAnswers } from '../../db/models/AccommAndTransport'
import { Guest } from '../../db/models/Guest'
import Button from '../Button'

const HYATT_PRINCETON = 'Hyatt Regency Princeton'

interface Props {
  guests: Guest[]
  answers: AccommAndTransportAnswers | null
}

const AccommAndTransportForm = ({ guests, answers }: Props) => {
  const router = useRouter()
  const [accommodations, setAccommodations] = useState(answers?.accommodations)
  const [shuttle, setShuttle] = useState(answers?.shuttle)
  const [brunch, setBrunch] = useState(answers?.brunch)
  const [freeform, setFreeform] = useState(answers?.freeform)
  const [answeringFor, setAnsweringFor] = useState(answers?.answering_for)

  const otherAccommodationsChecked =
    accommodations !== undefined && accommodations !== HYATT_PRINCETON
  const hasAnsweredAllRequiredQuestions =
    accommodations !== undefined &&
    accommodations !== '' &&
    shuttle !== undefined &&
    brunch !== undefined &&
    (guests || (answeringFor !== undefined && answeringFor !== ''))

  return (
    <>
      <form
        onSubmit={async (e) => {
          e.preventDefault()
          const body = {
            accommodations,
            shuttle,
            brunch,
            freeform,
            ...(!guests && { answeringFor }),
          }
          const response = await fetch('/api/accommodations-and-transportation', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body),
          })
          if (response.status === 204) {
            router.push('/forms/accommodations-and-transportation/thankyou')
          }
        }}
      >
        {guests ? (
          <p>
            You&apos;re answering for{' '}
            {guests.map((guest) => guest.preferredName ?? guest.name).join(', ')}
          </p>
        ) : (
          <div className="input-container">
            <label id="answering-for-label" htmlFor="answering-for">
              Who are you filling this form out for?
            </label>
            <textarea
              id="answering-for"
              name="answering-for"
              value={answeringFor}
              onChange={(e) => {
                setAnsweringFor(e.target.value)
              }}
            />
            <p className="input-description">
              If you&apos;re filling this out for your partner or family, please list the names of
              everyone attending, including your own!
            </p>
          </div>
        )}
        <fieldset>
          <legend>Where are you staying Saturday night, after the wedding?</legend>
          <div className="input-container">
            <input
              id="accommodations-hyatt-ptown-radio"
              name="accommodations"
              value={HYATT_PRINCETON}
              type="radio"
              checked={accommodations === HYATT_PRINCETON}
              onChange={(e) => {
                if (e.target.checked) {
                  setAccommodations(HYATT_PRINCETON)
                }
              }}
            ></input>
            <label id="accommodations-hyatt-ptown-label" htmlFor="accommodations-hyatt-ptown-radio">
              {HYATT_PRINCETON}
            </label>
          </div>
          <div className="input-container">
            <input
              id="accommodations-other-radio"
              name="accommodations"
              value="other"
              type="radio"
              checked={accommodations !== undefined && accommodations !== HYATT_PRINCETON}
              onChange={(e) => {
                if (e.target.checked) {
                  setAccommodations('')
                }
              }}
            ></input>
            <label id="accommodations-other-check-label" htmlFor="accommodations-other-radio">
              Other
            </label>
          </div>
          {otherAccommodationsChecked && (
            <input
              id="accommodations-other"
              name="accommodations-other"
              type="text"
              value={accommodations}
              onChange={(e) => {
                setAccommodations(e.target.value)
              }}
              placeholder="Where are you staying?"
            />
          )}
          <p className="input-description">
            This will help us plan for transportation needs! If you&apos;re staying at a different
            hotel or AirBnB in/around Hopewell, please let us know!
          </p>
        </fieldset>
        <fieldset>
          <legend>
            There will be a shuttle from the Hyatt Regency Princeton to the wedding venue (The
            Watershed Institute), and then back at the end of the night. Will you use it?
          </legend>
          <div className="input-container">
            <input
              id="shuttle-yes"
              name="shuttle"
              value="yes"
              type="radio"
              checked={shuttle === true}
              onChange={(e) => {
                if (e.target.checked) {
                  setShuttle(true)
                }
              }}
            ></input>
            <label id="shuttle-yes-label" htmlFor="shuttle-yes">
              Yes
            </label>
          </div>
          <div className="input-container">
            <input
              id="shuttle-no"
              name="shuttle"
              value="no"
              type="radio"
              checked={shuttle === false}
              onChange={(e) => {
                if (e.target.checked) {
                  setShuttle(false)
                }
              }}
            ></input>
            <label id="shuttle-no-label" htmlFor="shuttle-no">
              No
            </label>
          </div>
        </fieldset>
        <fieldset>
          <legend>
            There will be a brunch catered by{' '}
            <a href="https://www.jammincrepes.com/" rel="noreferrer noopener">
              Jammin&apos; Crepes
            </a>{' '}
            at our house on Sunday, the day after the wedding, at 11am. Do you want to come?
          </legend>
          <div className="input-container">
            <input
              id="brunch-yes"
              name="brunch"
              value="yes"
              type="radio"
              checked={brunch === true}
              onChange={(e) => {
                if (e.target.checked) {
                  setBrunch(true)
                }
              }}
            ></input>
            <label id="brunch-yes-label" htmlFor="brunch-yes">
              Yes
            </label>
          </div>
          <div className="input-container">
            <input
              id="brunch-no"
              name="brunch"
              type="radio"
              checked={brunch === false}
              onChange={(e) => {
                if (e.target.checked) {
                  setBrunch(false)
                }
              }}
            ></input>
            <label id="brunch-no-label" htmlFor="brunch-no">
              No
            </label>
          </div>
        </fieldset>
        <div className="input-container">
          <label htmlFor="freeform" id="freeform-label">
            Anything else you think we should know, or questions you have for us?
          </label>
          <textarea
            id="freeform"
            name="freeform"
            value={freeform}
            onChange={(e) => setFreeform(e.target.value)}
          ></textarea>
        </div>
        {!hasAnsweredAllRequiredQuestions && (
          <div>Please answer the first {guests ? 'three' : 'four'} questions!</div>
        )}
        <Button type="submit" disabled={!hasAnsweredAllRequiredQuestions}>
          Save
        </Button>
      </form>
      <style jsx>{`
        label {
          margin: 0 0.5rem;
        }

        fieldset {
          margin: 1rem 0;
        }
        .input-description {
          font-size: 0.75rem;
          margin: 0 0.5rem 0.5rem 0.5rem;
          color: gray;
        }
        .input-container {
          margin: 0.5rem 0;
        }
        textarea {
          width: 100%;
        }
      `}</style>
    </>
  )
}

export default AccommAndTransportForm
