import { useState } from 'react'
import css from 'styled-jsx/css'
import Button, { Variant } from '../Button'
import Dialog from '../Dialog'

interface ItemPurchasedDialogProps {
  stillNeededNumber: number
  initialQuantity?: number
  isOpen: boolean
  registryItemId: number
  onClose: () => void
  onUpdate: (quantity: number) => void
}

const flexChildCss = css.resolve`
  * {
    margin: 0 0.25rem;
  }
`

const ItemPurchasedDialog = ({ initialQuantity = 1, ...props }: ItemPurchasedDialogProps) => {
  const [quantity, setQuantity] = useState(initialQuantity)
  return (
    <>
      <Dialog isOpen={props.isOpen} ariaLabel="Did you purchase this?" onClose={props.onClose}>
        <p>Did you buy this for us?</p>
        <div>
          <span>
            <label className={flexChildCss.className} htmlFor="quantity">
              Quantity
            </label>
            <input
              type="number"
              className={flexChildCss.className}
              value={quantity}
              onChange={(e) => setQuantity(parseInt(e.target.value, 10))}
            />
          </span>
          <span>
            <Button
              className={flexChildCss.className}
              type="submit"
              onClick={async (e) => {
                e.preventDefault()
                const body = {
                  registryItemId: props.registryItemId,
                  quantity,
                }
                await fetch('/api/registry/item', {
                  method: 'PUT',
                  headers: {
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify(body),
                })
                props.onUpdate(quantity)
              }}
            >
              Submit
            </Button>
            <Button
              className={flexChildCss.className}
              type="button"
              variant={Variant.TERTIARY}
              onClick={async () => {
                if (0 !== initialQuantity) {
                  const body = {
                    registryItemId: props.registryItemId,
                    quantity: 0,
                  }
                  await fetch('/api/registry/item', {
                    method: 'PUT',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(body),
                  })
                }
                props.onUpdate(0)
              }}
            >
              I didn&apos;t buy this
            </Button>
          </span>
        </div>
      </Dialog>
      <style jsx>{`
        div {
          display: flex;
          width: 100%;
          justify-content: space-between;
        }

        span {
          align-self: center;
        }
      `}</style>
      {flexChildCss.styles}
    </>
  )
}

export default ItemPurchasedDialog
