import { useState, FC, Children } from 'react'
import Button from '../Button'
import Dialog from '../Dialog'

interface ExternalItemLinkProps {
  href: string
  registryItemId: number
  itemName: string
  storeName: string
  stillNeededNumber: number
  onPurchase: () => void
}

const ExternalItemLink: FC<ExternalItemLinkProps> = (props) => {
  const [isReminderDialogOpen, setReminderDialogOpen] = useState(false)
  const [hasUserClickedLink, setHasUserClickedLink] = useState(false)

  const isClickable = props.stillNeededNumber > 0 && props.href

  return (
    <>
      {Children.map(props.children, (child) => (
        // eslint-disable-next-line jsx-a11y/anchor-is-valid
        <a
          href={isClickable ? props.href : undefined}
          onClick={(e) => {
            if (isClickable) {
              e.preventDefault()
              setReminderDialogOpen(true)
            }
          }}
        >
          {child}
        </a>
      ))}
      <Dialog
        isOpen={isReminderDialogOpen}
        ariaLabel={props.itemName}
        onClose={() => {
          setReminderDialogOpen(false)
          setHasUserClickedLink(false)
        }}
      >
        {hasUserClickedLink ? (
          <>
            <p>
              If you ended up getting a {props.itemName} from {props.storeName}, please click the
              button below to let others know!
            </p>
            <Button
              type="button"
              onClick={() => {
                setReminderDialogOpen(false)
                props.onPurchase()
              }}
            >
              I bought this
            </Button>
          </>
        ) : (
          <>
            <p>
              Don&apos;t forget to come back and mark this item as bought if you end up deciding to
              buy it!
            </p>
            <p>
              <a
                href={props.href}
                rel="noopener noreferrer"
                target="_blank"
                onClick={() => {
                  setHasUserClickedLink(true)
                  const body = {
                    registryItemId: props.registryItemId,
                  }
                  fetch('/api/registry/click', {
                    method: 'PUT',
                    headers: {
                      'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(body),
                  })
                }}
              >
                Continue to {props.storeName}
              </a>
            </p>
          </>
        )}
      </Dialog>
      <style jsx>{`
        a {
          flex-basis: 2rem;
          flex-shrink: 0;
        }
      `}</style>
    </>
  )
}

export default ExternalItemLink
