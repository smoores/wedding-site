import { useState } from 'react'
import css from 'styled-jsx/css'
import { RegistryItemModel } from '../../db/models/RegistryItem'
import { secondaryBackgroundLighter } from '../../design/colors'
import Button, { Variant } from '../Button'
import ExternalItemLink from './ExternalItemLink'
import ItemPurchasedDialog from './ItemPurchasedDialog'

interface RegistryItemProps {
  item: RegistryItemModel
  className: string
}

const editButtonCss = css.resolve`
  * {
    margin-left: 1rem;
  }
`

const RegistryItem = (props: RegistryItemProps) => {
  const [isPurchasedDialogOpen, setPurchasedDialogOpen] = useState(false)
  const [isEditDialogOpen, setEditDialogOpen] = useState(false)
  const [stillNeededNumber, setStillNeededNumber] = useState(props.item.stillNeededNumber)
  const [quantityBoughtByCurrentUser, setQuantityBoughtByCurrentUser] = useState(
    props.item.quantityBoughtByCurrentUser
  )

  return (
    <>
      <li className={`${props.className} ${stillNeededNumber < 1 ? 'purchased' : ''}`}>
        <div className="image-wrapper">
          <img src={props.item.imageUrl} alt="" />
        </div>
        <ExternalItemLink
          href={props.item.storeUrl}
          registryItemId={props.item.id}
          itemName={props.item.name}
          storeName={props.item.storeName}
          stillNeededNumber={props.item.stillNeededNumber}
          onPurchase={() =>
            quantityBoughtByCurrentUser ? setEditDialogOpen(true) : setPurchasedDialogOpen(true)
          }
        >
          <p>{props.item.storeName}</p>
          <p className="item-name">{props.item.name}</p>
        </ExternalItemLink>
        <p>{props.item.price}</p>
        <p>Still Needs: {Math.max(0, stillNeededNumber)}</p>
        {quantityBoughtByCurrentUser ? (
          <div>
            You bought {quantityBoughtByCurrentUser}
            <Button
              className={editButtonCss.className}
              variant={Variant.SECONDARY}
              onClick={() => setEditDialogOpen(true)}
            >
              Edit quantity
            </Button>
            <ItemPurchasedDialog
              isOpen={isEditDialogOpen}
              onClose={() => setEditDialogOpen(false)}
              stillNeededNumber={stillNeededNumber + quantityBoughtByCurrentUser}
              initialQuantity={quantityBoughtByCurrentUser}
              registryItemId={props.item.id}
              onUpdate={(quantity) => {
                setStillNeededNumber(stillNeededNumber + quantityBoughtByCurrentUser - quantity)
                setQuantityBoughtByCurrentUser(quantity)
                setEditDialogOpen(false)
              }}
            />
          </div>
        ) : (
          <>
            <Button
              type="button"
              variant={stillNeededNumber > 0 ? Variant.PRIMARY : Variant.SECONDARY}
              onClick={() => setPurchasedDialogOpen(true)}
            >
              I bought this
            </Button>
            <ItemPurchasedDialog
              isOpen={isPurchasedDialogOpen}
              onClose={() => setPurchasedDialogOpen(false)}
              stillNeededNumber={stillNeededNumber}
              registryItemId={props.item.id}
              onUpdate={(quantity) => {
                setStillNeededNumber(stillNeededNumber - quantity)
                setQuantityBoughtByCurrentUser(quantity)
                setPurchasedDialogOpen(false)
              }}
            />
          </>
        )}
      </li>
      <style jsx>{`
        li {
          display: flex;
          flex-direction: column;
          flex-shrink: 0;
          flex-basis: 15rem;
          justify-content: space-between;
          text-align: left;
          position: relative;
          margin: 2rem;
        }

        li p {
          font-size: 0.8rem;
          margin: 0.25rem;
        }

        li p.item-name {
          font-size: 1rem;
        }

        .image-wrapper {
          display: flex;
          height: 15rem;
          margin-bottom: 0.5rem;
        }

        .image-wrapper img {
          display: block;
          border-radius: 0.25rem;
          max-width: 100%;
          max-height: 15rem;
          margin: auto;
        }

        .purchased::before {
          background-color: ${secondaryBackgroundLighter};
          width: calc(100% + 12px);
          height: 3rem;
          position: absolute;
          top: 10px;
          content: 'Purchased';
          display: flex;
          justify-content: space-around;
          align-items: center;
          left: -6px;
          font-size: 1.5rem;
          box-shadow: 0 2px 6px rgba(0, 0, 0, 0.12);
        }
      `}</style>
      {editButtonCss.styles}
    </>
  )
}

export default RegistryItem
