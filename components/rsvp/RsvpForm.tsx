import { useRouter } from 'next/router'
import { Guest } from '../../db/models/Guest'
import Button from '../Button'
import VaccinationNote from './VaccinationNote'

function slugify(str: string) {
  return str.toLowerCase().replace(' ', '-')
}

function getGuestSlug(guest: Guest) {
  return guest.isPlusOne ? 'plus-one' : slugify(guest.name)
}

interface RsvpFormProps {
  guests: Guest[]
}

const RsvpForm = ({ guests }: RsvpFormProps) => {
  const router = useRouter()
  return (
    <>
      <form
        onSubmit={async (e) => {
          e.preventDefault()
          const formData = new FormData(e.target as HTMLFormElement)
          const body = {
            guests: guests.map((guest) => {
              const slug = getGuestSlug(guest)
              return {
                id: guest.id,
                attending: formData.get(`${slug}-attending`) === 'on',
                name: guest.name,
                ...(guest.isPlusOne && {
                  name: formData.get(`${slug}-name`),
                }),
                dietaryRestrictions: formData.get(`${slug}-dietary-restrictions`),
                preferredName: guest.isPlusOne
                  ? formData.get(`${slug}-name`)
                  : formData.get(`${slug}-preferred-name`),
              }
            }, {}),
          }
          const response = await fetch('/api/rsvp', {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body),
          })
          if (response.status === 204) {
            router.push('/rsvp/thankyou')
          }
        }}
      >
        {guests.map((guest) => {
          const slug = getGuestSlug(guest)
          return (
            <fieldset key={guest.id}>
              <legend>{guest.isPlusOne ? '+1' : guest.name}</legend>
              {guest.isPlusOne && (
                <div>
                  <label id="plus-one-name-label" htmlFor="plus-one-name">
                    name
                  </label>
                  <input
                    type="text"
                    id="plus-one-name"
                    name="plus-one-name"
                    defaultValue={guest.name ?? ''}
                  ></input>
                </div>
              )}
              <div className="input-container">
                <label id={`${slug}-attending-label`} htmlFor={`${slug}-attending`}>
                  attending?
                </label>
                <input
                  id={`${slug}-attending`}
                  name={`${slug}-attending`}
                  type="checkbox"
                  defaultChecked={guest.attending}
                ></input>
              </div>
              {!guest.isPlusOne && (
                <div className="input-container">
                  <label id={`${slug}-preferred-name-label`} htmlFor={`${slug}-preferred-name`}>
                    preferred name
                  </label>
                  <input
                    id={`${slug}-preferred-name`}
                    name={`${slug}-preferred-name`}
                    type="text"
                    defaultValue={guest.preferredName}
                  ></input>
                  <p className="input-description">
                    this name will be used for your table card and thank you&apos;s, etc.
                  </p>
                </div>
              )}
              <div className="input-container">
                <label
                  id={`${slug}-dietary-restrictions-label`}
                  htmlFor={`${slug}-dietary-restrictions`}
                >
                  dietary restrictions?
                </label>
                <input
                  id={`${slug}-dietary-restrictions`}
                  name={`${slug}-dietary-restrictions`}
                  type="text"
                  placeholder="allergic to gluten, tree nuts."
                  defaultValue={guest.dietaryRestrictions ?? ''}
                ></input>
                <p className="input-description">
                  all of the food at our wedding will be vegan (and delicious)!
                </p>
              </div>
            </fieldset>
          )
        })}
        <VaccinationNote />
        <Button type="submit">Submit</Button>
      </form>
      <style jsx>{`
        label {
          margin: 0 0.5rem;
        }

        fieldset {
          margin: 1rem 0;
        }
        .input-description {
          font-size: 0.75rem;
          margin: 0 0.5rem 0.5rem 0.5rem;
          color: gray;
        }
        .input-container {
          margin: 0.5rem 0;
        }
      `}</style>
    </>
  )
}

export default RsvpForm
