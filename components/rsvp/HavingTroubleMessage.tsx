const HavingTroubleMessage = () => (
  <p>
    Having trouble with the form, or just don&apos;t want to bother with filling it out online? Give
    us a call instead, we&apos;d love to hear from you! Shane&apos;s phone number is{' '}
    <a href="tel:609-529-4876">(609) 529-4876</a> and Colleen&apos;s is{' '}
    <a href="tel:631-338-2666">(631) 338-2666</a>.
  </p>
)

export default HavingTroubleMessage
