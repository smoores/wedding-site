const ThankYouMessage = () => (
  <p>
    Thank you so much for letting us know whether you can make it to our wedding! You can update
    your RSVP any time before August 31st by navigating back to{' '}
    <a href="https://shaneandcolleengetmarried.com/rsvp">shaneandcolleengetmarried.com/rsvp</a>, so
    don&apos;t hesitate to let us know if something changes!
  </p>
)

export default ThankYouMessage
