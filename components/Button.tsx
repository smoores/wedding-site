import { ButtonHTMLAttributes, FC } from 'react'
import { primaryBackground, primaryBackgroundDark, primaryBackgroundDarker } from '../design/colors'

export enum Variant {
  PRIMARY = 'PRIMARY',
  SECONDARY = 'SECONDARY',
  TERTIARY = 'TERTIARY',
}

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  className?: string
  variant?: Variant
}

const Button: FC<ButtonProps> = ({ variant = Variant.PRIMARY, className = '', ...props }) => (
  <>
    <button className={`${className} ${variant.toLowerCase()}`} {...props} />
    <style jsx>{`
      button {
        font-family: Helvetica Neue;
        width: max-content;
        border: none;
        border-radius: 0.25rem;
        padding: 0.4rem 0.6rem;
        margin-top: 0.4rem;
        margin-bottom: 0.4rem;
        font-size: 0.8rem;
        cursor: pointer;
      }

      button:focus {
        outline: none;
      }

      button.primary {
        color: white;
        background: ${primaryBackground};
      }

      button.primary:hover:not(:disabled),
      button.primary:focus:not(:disabled) {
        background: ${primaryBackgroundDark};
      }

      button.primary:active:not(:disabled) {
        background: ${primaryBackgroundDarker};
      }

      button.secondary {
        color: inherit;
        background: white;
        border: 2px solid lightgrey;
      }

      button.secondary:hover:not(:disabled),
      button.secondary:focus:not(:disabled) {
        background: lightgrey;
        border-color: grey;
      }

      button.secondary:active:not(:disabled) {
        background: grey;
        border-color: darkgrey;
      }

      button.tertiary {
        color: inherit;
        background: white;
        padding: 0.25rem;
        text-decoration: underline;
      }

      button:disabled {
        opacity: 0.5;
      }
    `}</style>
  </>
)

export default Button
