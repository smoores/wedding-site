import { useRef, useMemo, useEffect, FunctionComponent } from 'react'
import ReactDOM from 'react-dom'
import FocusLock from 'react-focus-lock'

interface DialogProps {
  isOpen: boolean
  ariaLabel: string
  onClose: () => void
  className?: string
  fitContent?: boolean
}

const Dialog: FunctionComponent<DialogProps> = ({
  isOpen,
  ariaLabel,
  onClose,
  className,
  fitContent,
  children,
}) => {
  const priorActiveElementRef = useRef<HTMLElement>()
  const closeButtonRef = useRef<HTMLButtonElement>()
  const dialogParent = useRef<HTMLDivElement>()
  const portalContainer = useRef<HTMLDivElement>()

  // We want to grab the element that was used to open the DOM
  // _before_ React has a chance to execute any DOM mutations
  useMemo(() => {
    // The deps argument to useMemo is a recommendation, not a
    // guarantee, so we still have to do an explicit check here.
    if (!priorActiveElementRef.current && (isOpen || isOpen === undefined)) {
      priorActiveElementRef.current = document.activeElement as HTMLDivElement
    }
  }, [isOpen])

  useEffect(() => {
    if (isOpen || isOpen === undefined) {
      /* disable body scrolling while open */
      document.body.classList.add('scroll-locked')
    }
    /* reenable body scrolling when closed */
    return () => document.body.classList.remove('scroll-locked')
  }, [isOpen])

  useEffect(() => {
    if (isOpen || isOpen === undefined) {
      /* focuses DialogContent's Close button when modal is opened */
      if (!dialogParent.current?.contains(document.activeElement) && closeButtonRef.current) {
        closeButtonRef.current.focus()
      }
    }
    /* returns focus to document body after modal is closed */
    return () => {
      if (priorActiveElementRef.current && (isOpen || isOpen === undefined)) {
        priorActiveElementRef.current.focus()
        priorActiveElementRef.current = null
      }
    }
  }, [isOpen])

  useEffect(() => {
    /* Close modal esc is pressed */
    const onEscPressed = (e) => {
      if (e.key === 'Escape' || e.key === 'Esc') onClose()
    }
    if (isOpen || isOpen === undefined) document.addEventListener('keydown', onEscPressed)
    /* cleanup listener on componentDidUnmount */
    return () => document.removeEventListener('keydown', onEscPressed)
  }, [isOpen, onClose])

  useEffect(() => {
    return () => {
      if (portalContainer.current) {
        document.body.removeChild(portalContainer.current)
        portalContainer.current = null
      }
    }
  }, [])

  if (!isOpen) {
    return null
  }

  if (!portalContainer.current) {
    portalContainer.current = document.createElement('div')
    document.body.appendChild(portalContainer.current)
  }

  return ReactDOM.createPortal(
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions,jsx-a11y/click-events-have-key-events
    <div className="dialog-backdrop" onClick={onClose} ref={dialogParent}>
      {/* The event handler is only being used to stop bubbled events */}
      {/* See details at https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/no-static-element-interactions.md#case-the-event-handler-is-only-being-used-to-capture-bubbled-events */}
      {/* eslint-disable-next-line jsx-a11y/no-noninteractive-element-interactions,jsx-a11y/click-events-have-key-events */}
      <dialog
        open={isOpen}
        className={className}
        aria-modal="true"
        onClick={(e) => e.stopPropagation()}
        aria-label={ariaLabel}
      >
        <FocusLock>
          <button
            type="button"
            className="close-button"
            ref={closeButtonRef}
            onClick={onClose}
            aria-label="Close dialog"
          >
            ✕
          </button>
          {children}
        </FocusLock>
      </dialog>
      <style jsx>{`
        .dialog-backdrop {
          background: rgba(0, 0, 0, 0.3);
          display: flex;
          justify-content: center;
          position: fixed;
          overflow-y: auto;
          top: 0;
          right: 0;
          bottom: 0;
          left: 0;
          z-index: 999;
        }

        dialog {
          background: white;
          padding: 2rem;
          ${fitContent ? '' : 'height: fit-content;'}
          align-self: center;
          position: relative;
          ${fitContent ? '' : 'max-height: calc(100vh - 10rem);'}
          width: ${fitContent ? 'fit-content' : '600px'};
          overflow-y: auto;
          border: none;
          border-radius: 0.25rem;
          box-shadow: 0 2px 6px rgba(0, 0, 0, 0.12);
        }

        .close-button {
          position: absolute;
          top: 0.75rem;
          right: 0.75rem;
          border: none;
          background: none;
          font-size: 1rem;
          cursor: pointer;
        }

        :global(.scroll-locked) {
          overflow: hidden;
        }
      `}</style>
    </div>,
    portalContainer.current
  )
}

export default Dialog
