import { MutableRefObject, useEffect, useRef, useState } from 'react'

const backblazeEndpoint = 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery'

interface GalleryImageProps {
  id?: string
  filename: string
  onClick: () => void
}

export function GalleryImage({ id, filename, onClick }: GalleryImageProps) {
  const imageRef = useRef<HTMLDivElement | HTMLButtonElement | null>(null)
  const [showImage, setShowImage] = useState(false)
  useEffect(() => {
    function loadImageIfNearScreen() {
      if (!imageRef.current) return
      const imageRect = imageRef.current.getBoundingClientRect()
      const isNearScreen =
        imageRect.top - (window.screenTop + window.innerHeight) < 200 &&
        window.screenTop - imageRect.bottom < 200
      setShowImage(isNearScreen)
    }
    loadImageIfNearScreen()
    document.addEventListener('scroll', loadImageIfNearScreen)
    return () => document.removeEventListener('scroll', loadImageIfNearScreen)
  }, [])
  return (
    <>
      {showImage ? (
        <button ref={imageRef as MutableRefObject<HTMLButtonElement>} id={id} onClick={onClick}>
          <img
            srcSet={`${backblazeEndpoint}/300/${filename},`}
            src={`${backblazeEndpoint}/300/${filename}`}
            alt=""
            loading="lazy"
          ></img>
        </button>
      ) : (
        <div
          id={id}
          ref={imageRef as MutableRefObject<HTMLDivElement>}
          className="image-placeholder"
        />
      )}
      <style jsx>{`
        button,
        .image-placeholder {
          padding: 0;
          border: none;
          background: #f3f3f3;
          transition: opacity 0.5s;
          cursor: pointer;
          margin: 0;
          border-radius: 0.25rem;
          overflow: hidden;
          height: 240px;
          width: 100%;
        }

        button > img {
          height: 100%;
          width: 100%;
          object-fit: cover;
        }
      `}</style>
    </>
  )
}
