export interface AccommAndTransportAnswers {
  user_id: string
  accommodations: string
  shuttle: boolean
  brunch: boolean
  freeform: string
  answering_for: string
}
