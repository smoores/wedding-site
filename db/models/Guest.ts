export interface GuestDbRecord {
  id: number
  rsvp_code: string
  name: string
  attending: boolean
  is_plus_one: boolean
  dietary_restrictions: string
  preferred_name: string
  updated_at: Date
}

export interface Guest {
  id: number
  rsvpCode: string
  name: string
  attending: boolean
  isPlusOne: boolean
  dietaryRestrictions: string
  preferredName: string
  updatedAt: Date
}

export type SerializableGuest = Omit<Guest, 'updatedAt'> & { updatedAt: number }

export function buildGuests(records: GuestDbRecord[]): Guest[] {
  return records.map<Guest>(
    ({
      id,
      rsvp_code,
      name,
      attending,
      is_plus_one,
      dietary_restrictions,
      preferred_name,
      updated_at,
    }) => ({
      id,
      rsvpCode: rsvp_code,
      name,
      attending,
      isPlusOne: is_plus_one,
      dietaryRestrictions: dietary_restrictions,
      preferredName: preferred_name,
      updatedAt: updated_at,
    })
  )
}

export function buildSerializableGuests(guests: Guest[]): SerializableGuest[] {
  return guests.map((guest) => ({ ...guest, updatedAt: guest.updatedAt.valueOf() }))
}

export interface RsvpGroup {
  code: string
  guests: Guest[]
}

export function buildRsvpGroups(guests: Guest[]): RsvpGroup[] {
  const codeToGuestsMap = guests.reduce((acc, guest) => {
    if (!acc[guest.rsvpCode]) {
      return { ...acc, [guest.rsvpCode]: [guest] as Guest[] }
    }
    return { ...acc, [guest.rsvpCode]: [...acc[guest.rsvpCode], guest] }
  }, {} as Record<string, Guest[]>)
  return Object.entries(codeToGuestsMap)
    .map(([code, guests]) => ({ code, guests }))
    .sort((a, b) => {
      if (a.guests[0].attending !== null) {
        return -1
      }
      if (b.guests[0].attending !== null) {
        return 1
      }
      return 0
    })
}

export interface SerializableRsvpGroup {
  code: string
  guests: SerializableGuest[]
}

export function buildSerializableRsvpGroups(rsvpGroups: RsvpGroup[]): SerializableRsvpGroup[] {
  return rsvpGroups.map((group) => ({ ...group, guests: buildSerializableGuests(group.guests) }))
}
