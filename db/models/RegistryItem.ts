export interface RegistryItemDbRecord {
  id: number
  image_url?: string
  store_url?: string
  store_name?: string
  name: string
  price?: string
  requested_number: number
}

export interface RegistryItemToUserDbRecord extends RegistryItemDbRecord {
  quantity: number
  email: string
}

export interface RegistryItemModel {
  id: number
  imageUrl?: string
  storeUrl?: string
  storeName?: string
  name: string
  price?: string
  requestedNumber: number
  stillNeededNumber: number
  quantityBoughtByCurrentUser?: number
}

export function buildRegistryItems(
  itemRecords: RegistryItemToUserDbRecord[],
  user
): RegistryItemModel[] {
  return Object.values(
    itemRecords.reduce<Record<string, RegistryItemModel>>(
      (acc, itemRecord) => ({
        ...acc,
        [itemRecord.id]: {
          ...acc[itemRecord.id],
          id: itemRecord.id,
          imageUrl: itemRecord.image_url,
          storeUrl: itemRecord.store_url,
          storeName: itemRecord.store_name,
          name: itemRecord.name,
          price: itemRecord.price,
          requestedNumber: itemRecord.requested_number,
          stillNeededNumber:
            (acc[itemRecord.id]
              ? acc[itemRecord.id].stillNeededNumber
              : itemRecord.requested_number) - itemRecord.quantity,
          ...(itemRecord.email === user.email && {
            quantityBoughtByCurrentUser: itemRecord.quantity,
          }),
        },
      }),
      {}
    )
  ).sort((a, b) => {
    const dollarsA = parseFloat((a.price ?? '$0').slice(1))
    const dollarsB = parseFloat((b.price ?? '$0').slice(1))
    return dollarsA - dollarsB
  })
}
