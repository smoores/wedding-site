import { AdapterUser } from 'next-auth/adapters'

export interface UserDbRecord {
  id: string
  name: string
  email: string
  emailVerified: number
  created_at: string
  updated_at: string
  rsvp_code: string
  password_hash: string
}

export interface User extends AdapterUser {
  createdAt: string
  updatedAt: string
  rsvpCode: string
  passwordHash: string
}

export function buildUser(userRecord: UserDbRecord): User {
  return {
    id: userRecord.id,
    name: userRecord.name,
    email: userRecord.email,
    emailVerified: new Date(userRecord.emailVerified),
    createdAt: userRecord.created_at,
    updatedAt: userRecord.updated_at,
    rsvpCode: userRecord.rsvp_code,
    passwordHash: userRecord.password_hash,
    image: null,
    sessions: [],
    accounts: [],
  }
}
