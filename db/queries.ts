import { AdapterSession, VerificationToken } from 'next-auth/adapters'
import { getConnection } from './'
import { AccommAndTransportAnswers } from './models/AccommAndTransport'
import { buildGuests, Guest } from './models/Guest'
import { RegistryItemToUserDbRecord } from './models/RegistryItem'
import { buildUser, User } from './models/User'

export async function getUserIdByEmail(email: string) {
  const connection = await getConnection()
  const {
    rows: [{ id }],
  } = await connection.query(`SELECT id FROM users WHERE email=$1`, [email])
  return id as string
}

export async function createUser(email: string, passwordHash: string) {
  const connection = await getConnection()
  if (passwordHash) {
    return connection.query(
      `
        INSERT INTO users (email, password_hash)
        VALUES ($1, $2)
      `,
      [email, passwordHash]
    )
  }
  return connection.query(
    `
      INSERT INTO users (email)
      VALUES ($1)
    `,
    [email, passwordHash]
  )
}

export async function updateUserPasswordHash(id: string, passwordHash: string) {
  const connection = await getConnection()
  return connection.query(
    `
      UPDATE users
      SET password_hash = $1
      WHERE id = $2
    `,
    [passwordHash, id]
  )
}

export async function getUserByEmail(email: string) {
  console.log(email)
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT *
      FROM users
      WHERE email = $1
    `,
    [email]
  )
  return results.rows[0] ? buildUser(results.rows[0]) : null
}

export async function getUserById(id: string): Promise<User> {
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT *
      FROM users
      WHERE id = $1
    `,
    [id]
  )
  return results.rows[0] ? buildUser(results.rows[0]) : null
}

export async function updateUser(user: Partial<User>) {
  const connection = await getConnection()
  const mapped = { ...user, emailVerified: user.emailVerified?.valueOf().toString() }
  const columnValues = Object.entries(mapped).filter(([column]) => column !== 'id')
  return connection.query(
    `
      UPDATE users
      SET ${columnValues.map(([column], i) => `"${column}" = $${i + 2}`)}
      WHERE id = $1
    `,
    [user.id, columnValues.map(([, value]) => value)]
  )
}

export async function createSession({ sessionToken, userId, expires }: Omit<AdapterSession, 'id'>) {
  const connection = await getConnection()
  return connection.query(
    `
      INSERT INTO sessions
      ("sessionToken", "userId", expires)
      VALUES ($1, $2, $3)
    `,
    [sessionToken, userId, expires.valueOf()]
  )
}

export async function getSession(sessionToken: string): Promise<AdapterSession> {
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT *
      FROM sessions
      WHERE "sessionToken" = $1
    `,
    [sessionToken]
  )
  return {
    ...results.rows[0],
    id: results.rows[0]?.id.toString(),
    expires: new Date(results.rows[0]?.expires),
  }
}

export async function updateSession(session: { sessionToken: string } & Partial<AdapterSession>) {
  const connection = await getConnection()
  const columnValues = Object.entries(session).filter(([column]) => column !== 'sessionToken')
  return connection.query(
    `
      UPDATE sessions
      SET ${columnValues.map(([column], i) => `${column} = $${i + 2}`)}
      WHERE "sessionToken" = $1
    `,
    [session.sessionToken, columnValues.map(([, value]) => value)]
  )
}

export async function deleteSession(sessionToken: string) {
  const connection = await getConnection()
  return connection.query(
    `
    DELETE FROM sessions
    WHERE "sessionToken" = $1
  `,
    [sessionToken]
  )
}

export async function createVerificationToken(verificationToken: VerificationToken) {
  const connection = await getConnection()
  return connection.query(
    `
      INSERT INTO verification_tokens
      (identifier, token, expires)
      VALUES ($1, $2, $3)
    `,
    [verificationToken.identifier, verificationToken.token, verificationToken.expires.valueOf()]
  )
}

export async function getVerificationToken(
  identifier: string,
  token: string
): Promise<VerificationToken> {
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT *
      FROM verification_tokens
      WHERE identifier = $1 AND token = $2
    `,
    [identifier, token]
  )
  return results.rows[0] && { ...results.rows[0], expires: new Date(results.rows[0].token) }
}

export async function deleteVerificationToken(identifier: string, token: string) {
  const connection = await getConnection()
  return connection.query(
    `
      DELETE FROM verification_tokens
      WHERE identifier = $1 AND token = $2
    `,
    [identifier, token]
  )
}

export async function updateRegistryItemPurchasedQuantity(
  registryItemId: number,
  userId: string,
  quantity: number
) {
  const connection = await getConnection()
  return connection.query(
    `
      INSERT INTO registry_items_to_users (registry_item_id, user_id, quantity)
      VALUES ($1, $2, $3)
      ON CONFLICT ON CONSTRAINT registry_items_to_users_pkey
      DO UPDATE SET quantity=$3
    `,
    [registryItemId, userId, quantity]
  )
}

export async function incrementRegistryItemNumberOfClicks(regitsryItemId: number, userId) {
  const connection = await getConnection()
  return connection.query(
    `
      INSERT INTO registry_items_to_users (registry_item_id, user_id, quantity, number_of_clicks)
      VALUES ($1, $2, 0, 1)
      ON CONFLICT ON CONSTRAINT registry_items_to_users_pkey
      DO UPDATE SET number_of_clicks=(
        SELECT number_of_clicks FROM registry_items_to_users
        WHERE registry_item_id=$1 AND user_id=$2
      ) + 1
    `,
    [regitsryItemId, userId]
  )
}

export async function getRegistryItemsToUsers(): Promise<Array<RegistryItemToUserDbRecord>> {
  const connection = await getConnection()
  const result = await connection.query(`
    SELECT
      registry_items.*,
      registry_items_to_users.quantity,
      users.email
    FROM registry_items
    LEFT JOIN registry_items_to_users
    ON registry_items.id = registry_items_to_users.registry_item_id
    LEFT JOIN users
    ON registry_items_to_users.user_id = users.id
  `)
  return result.rows
}

export async function getRsvpCodeByEmail(email: string) {
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT rsvp_code
      FROM users
      WHERE email = $1
    `,
    [email]
  )
  return results.rows[0].rsvp_code as string
}

export async function getIsAdminByEmail(email: string) {
  const connection = await getConnection()
  const results = await connection.query(
    `
      SELECT is_admin
      FROM users
      WHERE email = $1
    `,
    [email]
  )
  return results.rows[0].is_admin as boolean
}

export async function updateRsvpCodeByEmail(rsvpCode: string, email: string) {
  const connection = await getConnection()
  return connection.query(
    `
    UPDATE users
    SET rsvp_code = $1
    WHERE email = $2
  `,
    [rsvpCode, email]
  )
}

export async function getGuestsForRsvpCode(rsvpCode: string) {
  const connection = await getConnection()
  const result = await connection.query(
    `
      SELECT id, rsvp_code, name, attending, is_plus_one, dietary_restrictions, preferred_name
      FROM guests
      WHERE rsvp_code = $1
    `,
    [rsvpCode]
  )
  return buildGuests(result.rows)
}

export async function getAccommAndTransportAnswersByEmail(
  userId: string
): Promise<AccommAndTransportAnswers[]> {
  const connection = await getConnection()
  const result = await connection.query(
    `
      SELECT user_id, accommodations, shuttle, brunch, freeform, answering_for
      FROM accomm_and_transport
      JOIN users on accomm_and_transport.user_id = users.id
      WHERE users.email = $1
    `,
    [userId]
  )
  return result.rows
}

export async function setAccommAndTransportAnswers(
  userId: string,
  accommodations: string,
  shuttle: boolean,
  brunch: boolean,
  freeform: string,
  answeringFor: string
) {
  const connection = await getConnection()
  await connection.query(
    `
    INSERT INTO accomm_and_transport (
      user_id,
      accommodations,
      shuttle,
      brunch,
      freeform,
      answering_for
    )
    VALUES ($1, $2, $3, $4, $5, $6)
    ON CONFLICT ON CONSTRAINT aat_user_id_unique
    DO UPDATE SET
      accommodations=$2,
      shuttle=$3,
      brunch=$4,
      freeform=$5,
      answering_for=$6
  `,
    [userId, accommodations, shuttle, brunch, freeform, answeringFor]
  )
}

export async function getAllGuests() {
  const connection = await getConnection()
  const result = await connection.query(
    `
      SELECT id, rsvp_code, name, attending, is_plus_one, dietary_restrictions, preferred_name, updated_at
      FROM guests
    `
  )
  return buildGuests(result.rows)
}

export async function updateGuests(guests: Array<Partial<Guest>>) {
  const connection = await getConnection()
  return connection.query(
    `
    UPDATE guests
    SET
      name = guest_updates.name,
      attending = guest_updates.attending,
      dietary_restrictions = guest_updates.dietary_restrictions,
      preferred_name = guest_updates.preferred_name
    FROM (
      VALUES
        ${guests.map(
          (guest, index) => `(
            $${index * 5 + 1}::integer,
            $${index * 5 + 2},
            $${index * 5 + 3}::boolean,
            $${index * 5 + 4},
            $${index * 5 + 5}
          )`
        )}
    ) as guest_updates (id, name, attending, dietary_restrictions, preferred_name)
    WHERE guests.id = guest_updates.id
  `,
    guests.reduce(
      (acc, guest) => [
        ...acc,
        guest.id,
        guest.name,
        guest.attending,
        guest.dietaryRestrictions,
        guest.preferredName,
      ],
      []
    )
  )
}

export async function createRsvp(code: string, guests: Array<Pick<Guest, 'name' | 'isPlusOne'>>) {
  const connection = await getConnection()
  await connection.query(
    `
    INSERT INTO rsvps
      (code)
    VALUES
      ($1)
  `,
    [code]
  )
  return connection.query(
    `
    INSERT INTO guests
      (name, is_plus_one, rsvp_code)
    VALUES
      ${guests.map(
        (guest, index) => `(
          $${index * 3 + 1},
          $${index * 3 + 2}::boolean,
          $${index * 3 + 3}
        )`
      )}
  `,
    guests.reduce((acc, guest) => [...acc, guest.name, guest.isPlusOne, code], [])
  )
}
