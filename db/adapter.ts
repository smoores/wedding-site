import { hash } from 'argon2'
import { Adapter } from 'next-auth/adapters'
import {
  createSession,
  createUser,
  createVerificationToken,
  deleteSession,
  deleteVerificationToken,
  getSession,
  getUserByEmail,
  getUserById,
  getVerificationToken,
  updateSession,
  updateUser,
} from './queries'

export const PostgresAdapter: Partial<Adapter> = {
  async createUser(user: { email: string; password: string | null }) {
    const passwordHash = user.password
      ? await hash(user.password, {
          secret: Buffer.from(process.env.NEXTAUTH_SECRET),
        })
      : null
    await createUser(user.email, passwordHash)
    const { passwordHash: _, ...userResult } = await getUserByEmail(user.email)
    return { ...userResult, emailVerified: new Date(userResult.emailVerified) }
  },
  getUser: getUserById,
  getUserByEmail,
  async updateUser(user) {
    console.log(user)
    await updateUser(user)
    const { passwordHash: _, ...userResult } = await getUserById(user.id)
    return { ...userResult, emailVerified: new Date(userResult.emailVerified) }
  },
  async createSession({ sessionToken, userId, expires }) {
    await createSession({ sessionToken, userId, expires })
    return getSession(sessionToken)
  },
  async getSessionAndUser(sessionToken) {
    const session = await getSession(sessionToken)
    const { passwordHash: _, ...userResult } = await getUserById(session.userId)
    return { user: { ...userResult, emailVerified: new Date(userResult.emailVerified) }, session }
  },
  async updateSession(session) {
    await updateSession(session)
    return getSession(session.sessionToken)
  },
  async deleteSession(sessionToken) {
    const session = await getSession(sessionToken)
    await deleteSession(sessionToken)
    return session
  },
  async createVerificationToken(verificationToken) {
    await createVerificationToken(verificationToken)
    return getVerificationToken(verificationToken.identifier, verificationToken.token)
  },
  async useVerificationToken({ identifier, token }) {
    const verificationToken = await getVerificationToken(identifier, token)
    if (verificationToken) {
      await deleteVerificationToken(identifier, token)
      return verificationToken
    }
    return null
  },
}
