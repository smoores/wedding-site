import { Client } from 'pg'
export const options = {
  host: process.env.DB_HOST,
  port: parseInt(process.env.DB_PORT, 10),
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
}

let client: Client | null = null

export async function getConnection() {
  if (!client) {
    client = new Client(options)
    await client.connect()
  }
  return client
}
