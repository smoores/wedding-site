import { GetServerSidePropsContext } from 'next'
import { NextAuthHandler } from 'next-auth/core'
import { options } from '../pages/api/auth/[...nextauth]'
import { setCookie } from './setCookie'

export async function getServerCsrfToken(
  context: GetServerSidePropsContext
): Promise<string | null> {
  const session = await NextAuthHandler<{ csrfToken: string }>({
    options: {
      ...options,
      secret: options.secret ?? process.env.NEXTAUTH_SECRET,
    },
    req: {
      host: process.env.NEXTAUTH_URL,
      action: 'csrf',
      method: 'GET',
      cookies: context.req.cookies,
      headers: context.req.headers,
    },
  })

  const { body, cookies } = session

  cookies?.forEach((cookie) => setCookie(context.res, cookie))

  return body?.csrfToken ?? null
}
