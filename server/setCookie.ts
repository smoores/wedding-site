import { serialize } from 'cookie'
import { ServerResponse } from 'http'
import { Cookie } from 'next-auth/core/lib/cookie'

export function setCookie(res: ServerResponse, cookie: Cookie) {
  // Preserve any existing cookies that have already been set in the same session
  const setCookieHeader = res.getHeader('Set-Cookie') ?? []
  // If not an array (i.e. a string with a single cookie) convert it into an array
  const setCookieHeaderArray = Array.isArray(setCookieHeader)
    ? setCookieHeader
    : [setCookieHeader.toString()]
  const { name, value, options } = cookie
  const cookieHeader = serialize(name, value, options)
  setCookieHeaderArray.push(cookieHeader)
  res.setHeader('Set-Cookie', setCookieHeader)
}
