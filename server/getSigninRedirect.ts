import { GetServerSidePropsContext, PreviewData, Redirect } from 'next'
import { ParsedUrlQuery } from 'querystring'

export async function getSigninRedirect(
  context: GetServerSidePropsContext<ParsedUrlQuery, PreviewData>
): Promise<Redirect | null> {
  const destination = new URL('/auth/signin', process.env.NEXTAUTH_URL)
  const callbackUrl = new URL(context.resolvedUrl, process.env.NEXTAUTH_URL)
  const encodedCallbackUrl = encodeURI(callbackUrl.toString())
  destination.searchParams.append('callbackUrl', encodedCallbackUrl)

  return {
    destination: destination.toString(),
    permanent: false,
  }
}
