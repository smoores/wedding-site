/** pallete created on palleton.com */
export const primaryBackground = '#28546C'
export const primaryBackgroundDark = '#113B51'
export const primaryBackgroundDarker = '#032436'
export const primaryBackgroundLight = '#477187'
export const primaryBackgroundLighter = '#6F90A2'

export const secondaryBackground = '#AA5239'
export const secondaryBackgroundDark = '#802D15'
export const secondaryBackgroundDarker = '#551300'
export const secondaryBackgroundLight = '#D4826A'
export const secondaryBackgroundLighter = '#FFBDAA'

export const tertiaryBackground = '#AA8B39'
export const tertiaryBackgroundDark = '#806315'
export const tertiaryBackgroundDarker = '#553E00'
export const tertiaryBackgroundLight = '#D4B86A'
export const tertiaryBackgroundLighter = '#FFE8AA'
