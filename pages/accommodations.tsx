const AccommodationsPage = () => {
  return (
    <>
      <main>
        <header>
          <h1>Accommodations</h1>
        </header>
        <h2>Hyatt Regency Princeton</h2>
        <img
          alt=""
          src="https://assets.hyatt.com/content/dam/hyatt/hyattdam/images/2021/11/15/1500/PRINC-P0112-Dining-Atrium.jpg/PRINC-P0112-Dining-Atrium.16x9.jpg?imwidth=1920"
        />
        <address>
          Hyatt Regency Princeton
          <br />
          102 Carnegie Center,
          <br />
          Princeton, New Jersey, 08540-6293
        </address>
        <p>
          We&apos;ve reserved a block of rooms at the Hyatt Regency in Princeton. If you&apos;d like
          to book a room in the block, you can do so with{' '}
          <a
            href="https://www.hyatt.com/en-US/group-booking/PRINC/G-HIGG"
            rel="noopener noreferrer"
          >
            this link
          </a>
          , or you can call the hotel reservations department at{' '}
          <a href="tel:+18778037534">(877) 803-7534</a> and mention that you&apos;re with the
          Higgins-Moore Wedding Room Block or use the code <strong>G-HIGG</strong>. You{' '}
          <em>must</em> make reservations before September 8th!
        </p>
        <h2>Travel to and from the hotel</h2>
        <p>
          The Hyatt has a free shuttle to take you anywhere within 5 miles of the hotel, including
          downtown Princeton and the Princeton Junction train station.
        </p>
        <h3>If you&apos;re flying</h3>
        <p>
          We recommend flying in to the Newark International Airport. From there, you can take the
          NJ Transit train to Princeton Junction Station, via the Newark Airport AirTrain. You can
          see more information about the AirTrain{' '}
          <a href="https://www.airport-ewr.com/airtrain-newark.php" rel="noopener noreferrer">
            here
          </a>
          . You can then take the hotel shuttle from the train station to the hotel.
        </p>
        <h2>Travel to the wedding</h2>
        <p>The wedding ceremony and reception will both be at the Watershed Institute:</p>
        <address>
          The Watershed Institute
          <br />
          31 Titus Mill Rd
          <br />
          Pennington, New Jersey 08534
        </address>
        <p>
          There will be a shuttle from the Hyatt to the Watershed Institute before the ceremony
          begins, and back to the Hyatt after the reception. The drive to the Watershed is about
          twenty minutes.
        </p>
      </main>
      <style jsx>{`
        main {
          max-width: 48rem;
          margin: auto;
        }

        main > * {
          width: 85%;
          margin-left: auto;
          margin-right: auto;
        }

        header {
          text-align: center;
        }

        img {
          display: block;
        }

        h2,
        h3 {
          text-transform: none;
        }
      `}</style>
    </>
  )
}

export default AccommodationsPage
