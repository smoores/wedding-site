import Head from 'next/head'
import { useState } from 'react'
import css from 'styled-jsx/css'
import Button from '../components/Button'
import Dialog from '../components/Dialog'
import { GalleryImage } from '../components/gallery/GalleryImage'
import { weddingDayPhotoIds } from '../weddingDayPhotoIds'

const backblazeEndpoint = 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery'

const photoShootOneIds: [number, 'portrait' | 'landscape'][] = [
  [5, 'portrait'],
  [8, 'portrait'],
  [20, 'portrait'],
  [43, 'portrait'],
  [47, 'portrait'],
  [53, 'landscape'],
  [61, 'portrait'],
  [67, 'portrait'],
  [82, 'landscape'],
  [90, 'portrait'],
  [97, 'portrait'],
  [101, 'portrait'],
  [109, 'landscape'],
  [120, 'portrait'],
  [137, 'portrait'],
  [139, 'landscape'],
  [145, 'portrait'],
  [168, 'portrait'],
  [171, 'portrait'],
  [177, 'portrait'],
  [180, 'portrait'],
  [184, 'portrait'],
  [187, 'portrait'],
  [211, 'portrait'],
  [217, 'portrait'],
  [244, 'portrait'],
  [247, 'portrait'],
  [250, 'portrait'],
  [259, 'portrait'],
  [260, 'portrait'],
  [263, 'portrait'],
  [271, 'landscape'],
  [272, 'landscape'],
  [283, 'landscape'],
  [285, 'landscape'],
  [293, 'landscape'],
  [311, 'landscape'],
  [316, 'portrait'],
  [318, 'landscape'],
]

const photoShootTwoIds: [number, 'portrait' | 'landscape'][] = [
  [4, 'landscape'],
  [6, 'landscape'],
  [7, 'landscape'],
  [8, 'landscape'],
  [10, 'landscape'],
  [13, 'landscape'],
  [14, 'landscape'],
  [15, 'landscape'],
  [16, 'landscape'],
  [17, 'landscape'],
  [20, 'landscape'],
  [24, 'landscape'],
  [26, 'landscape'],
  [27, 'landscape'],
  [31, 'landscape'],
  [32, 'landscape'],
  [33, 'landscape'],
  [36, 'landscape'],
  [37, 'landscape'],
  [39, 'landscape'],
  [41, 'landscape'],
  [42, 'landscape'],
  [43, 'landscape'],
  [44, 'portrait'],
  [47, 'landscape'],
  [48, 'portrait'],
  [50, 'portrait'],
  [51, 'portrait'],
  [53, 'portrait'],
  [58, 'portrait'],
  [59, 'landscape'],
  [62, 'landscape'],
  [63, 'landscape'],
  [65, 'landscape'],
  [66, 'landscape'],
  [67, 'landscape'],
  [69, 'landscape'],
  [70, 'landscape'],
  [72, 'landscape'],
  [74, 'landscape'],
  [78, 'landscape'],
  [79, 'landscape'],
  [81, 'portrait'],
  [82, 'portrait'],
  [84, 'portrait'],
  [85, 'portrait'],
  [87, 'landscape'],
  [90, 'landscape'],
  [95, 'portrait'],
  [96, 'portrait'],
  [100, 'landscape'],
  [101, 'landscape'],
  [102, 'landscape'],
  [103, 'landscape'],
  [107, 'landscape'],
  [108, 'landscape'],
  [109, 'landscape'],
  [113, 'landscape'],
  [115, 'landscape'],
  [117, 'landscape'],
  [120, 'portrait'],
  [123, 'portrait'],
  [124, 'landscape'],
  [126, 'landscape'],
  [127, 'landscape'],
  [132, 'landscape'],
  [134, 'portrait'],
  [140, 'landscape'],
  [142, 'landscape'],
  [143, 'landscape'],
  [145, 'landscape'],
  [146, 'landscape'],
  [147, 'landscape'],
]

const dialogCss = css.resolve`
  * {
    min-width: 10rem;
  }
`

const Gallery = () => {
  const [selectedImageFilename, setSelectedImageFilename] = useState<string | null>(null)
  const [isImageLoading, setIsImageLoading] = useState(false)

  return (
    <>
      <Head>
        <title>Shane &amp; Colleen&apos;s Photo Gallery</title>
      </Head>
      <main>
        <header>
          <h1 id="heading">Shane &amp; Colleen&apos;s Photo Gallery</h1>
          <nav className="inner-nav">
            <a href="#heading">Wedding&nbsp;Photos</a>{' '}
            <a href="#freedom-farm-start">Engagement&nbsp;Photos&nbsp;@&nbsp;Freedom&nbsp;Farm</a>{' '}
            <a href="#botanic-gardens-start">
              Engagement&nbsp;Photos&nbsp;@&nbsp;Botanic&nbsp;Gardens
            </a>
          </nav>
        </header>
        <div className="gallery">
          {weddingDayPhotoIds.map(([id, orientation]) => (
            <div key={`3:${id}`} className={`thumbnail-wrapper ${orientation}`}>
              <GalleryImage
                filename={`cs-${id}.webp`}
                onClick={() => {
                  setIsImageLoading(true)
                  setSelectedImageFilename(`cs-${id}.webp`)
                }}
              />
            </div>
          ))}

          {photoShootTwoIds.map(([id, orientation], index) => (
            <div key={`2:${id}`} className={`thumbnail-wrapper ${orientation}`}>
              {index === 0 && (
                <aside className="freedom-farm-aside">
                  <h2>
                    <a
                      href="https://www.facebook.com/freedomfarmanimalrescue/"
                      rel="noopener noreferrer"
                    >
                      Freedom Farm
                    </a>
                  </h2>
                  <p>
                    Cedarville, NJ
                    <br />
                    Photography by Justin Pedrick
                    <br />
                    <a href="https://www.justinpedrick.com/" rel="noopener">
                      www.justinpedrick.com
                    </a>
                  </p>
                </aside>
              )}
              <GalleryImage
                id={index === 0 ? 'freedom-farm-start' : undefined}
                filename={`c%2Bs-${id}.jpg`}
                onClick={() => {
                  setIsImageLoading(true)
                  setSelectedImageFilename(`c%2Bs-${id}.jpg`)
                }}
              />
            </div>
          ))}

          {photoShootOneIds.map(([id, orientation], index) => (
            <div key={`2:${id}`} className={`thumbnail-wrapper ${orientation}`}>
              {index === 0 && (
                <aside id="botanic-gardens-aside" className="botanic-gardens-aside">
                  <h2>Cornell Botanic Gardens</h2>
                  <p>
                    Ithaca, NY
                    <br />
                    Photography by Danielle Gerritsen
                    <br />
                    <a href="https://www.gerritsenphotography.com/" rel="noopener">
                      www.gerritsenphotography.com
                    </a>
                  </p>
                </aside>
              )}
              <GalleryImage
                id={index === 0 ? 'botanic-gardens-start' : undefined}
                filename={`Shane-${id}.jpg`}
                onClick={() => {
                  setIsImageLoading(true)
                  setSelectedImageFilename(`Shane-${id}.jpg`)
                }}
              />
            </div>
          ))}
        </div>
      </main>
      <Dialog
        className={dialogCss.className}
        isOpen={!!selectedImageFilename}
        ariaLabel="Viewing a specific gallery image"
        onClose={() => setSelectedImageFilename(null)}
        fitContent
      >
        {isImageLoading && <span className="loading">Loading...</span>}
        <img
          className="fullsize"
          src={`${backblazeEndpoint}/fullsize/${selectedImageFilename}`}
          alt=""
          onLoad={() => setIsImageLoading(false)}
        />
        {!isImageLoading && (
          <a
            href={`${backblazeEndpoint}/original/${selectedImageFilename?.replace(
              '.webp',
              '.jpg'
            )}`}
            download
          >
            <Button>Download</Button>
          </a>
        )}
      </Dialog>
      <style jsx>{`
        main {
          margin: auto;
          max-width: 57rem;
          width: 100%;
        }

        main > header {
          text-align: center;
        }

        .inner-nav {
          font-size: 0.8em;
        }

        .gallery {
          position: relative;
          padding: 0.5rem;
          display: grid;
          grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
          grid-gap: 0.5rem;
          grid-auto-rows: 240px;
          grid-auto-flow: dense;
        }

        aside {
          text-align: left;
        }

        button {
          padding: 0;
          border: none;
          background: none;
          transition: opacity 0.5s;
          cursor: pointer;
          margin: 0;
          border-radius: 0.25rem;
          overflow: hidden;
          height: 240px;
        }

        button > img {
          height: 100%;
          width: 100%;
          object-fit: cover;
        }

        .thumbnail-wrapper.landscape {
          grid-column-end: span 2;
        }

        main:hover > button:not(:hover),
        main:focus-within > button:not(:focus) {
          opacity: 0.8;
        }

        img.fullsize {
          display: block;
          max-height: calc(100vh - 8rem);
        }

        .loading {
          position: absolute;
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        }

        @media (max-width: 480px) {
          img.fullsize {
            max-height: calc(100vh - 4rem);
            max-width: calc(100vw - 4rem);
          }
        }

        aside {
          display: none;
        }

        @media (min-width: 1640px) {
          aside {
            display: block;
            position: absolute;
          }

          .freedom-farm-aside {
            transform: translateX(115%);
          }

          .botanic-gardens-aside {
            transform: translateX(60%);
          }
        }
      `}</style>
      {dialogCss.styles}
    </>
  )
}

export default Gallery
