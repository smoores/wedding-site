import Document, { Html, Head, Main, NextScript } from 'next/document'

class WeddingSiteDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          <meta property="og:title" content="Shane &amp; Colleen Get Married" />
          <meta property="og:type" content="website" />
          <meta property="og:url" content="https://www.shaneandcolleengetmarried.com/" />
          <meta
            property="og:image"
            content="https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/500/Shane-247.jpg"
          />
          <link rel="icon" href="/favicon.png" />
        </Head>
        <body>
          <Main />
          <NextScript />
          <script
            dangerouslySetInnerHTML={{
              __html: `(function(f, a, t, h, o, m){
                a[h]=a[h]||function(){
                  (a[h].q=a[h].q||[]).push(arguments)
                };
                o=f.createElement('script'),
                m=f.getElementsByTagName('script')[0];
                o.async=1; o.src=t; o.id='fathom-script';
                m.parentNode.insertBefore(o,m)
              })(document, window, '/fathom/tracker.js', 'fathom');
              fathom('set', 'siteId', 'ONXCR');
              fathom('trackPageview');`,
            }}
          >
            {}
          </script>
        </body>
      </Html>
    )
  }
}

export default WeddingSiteDocument
