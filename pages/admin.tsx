import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { signIn, useSession } from 'next-auth/react'
import Head from 'next/head'
import Link from 'next/link'
import Button from '../components/Button'
import {
  buildRsvpGroups,
  buildSerializableGuests,
  buildSerializableRsvpGroups,
  Guest,
  RsvpGroup,
} from '../db/models/Guest'
import { getAllGuests, getIsAdminByEmail } from '../db/queries'
import { getSigninRedirect } from '../server/getSigninRedirect'
import { options } from './api/auth/[...nextauth]'

interface AdminPageProps {
  rsvps: RsvpGroup[]
  guests: Guest[]
}

const AdminPage = ({ rsvps, guests }: AdminPageProps) => {
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
      </>
    )
  }

  const haveAnswered = guests.filter((guest) => guest.attending !== null)
  const areAttending = guests.filter((guest) => guest.attending)
  const areNotAttending = guests.filter((guest) => guest.attending === false)

  return (
    <>
      <Head>
        <title>Admin - Shane &amp; Colleen Get Married</title>
      </Head>
      <header>
        <h1>Admin Page</h1>
      </header>
      <main>
        <section>
          <header>
            <h2>RSVPs</h2>
          </header>
          <p>{areAttending.length} guests are attending</p>
          <p>{areNotAttending.length} guests are not attending</p>
          <p>{haveAnswered.length} guests have RSVPed</p>
          <p>{guests.length - haveAnswered.length} guests have not yet RSVPed</p>
          <details>
            <summary>all rsvp info</summary>
            <table>
              <thead>
                <tr>
                  <th>rsvp code</th>
                  <th>name</th>
                  <th>preferred name</th>
                  <th>attending</th>
                  <th>+1</th>
                  <th>dietary restrictions</th>
                </tr>
              </thead>
              {rsvps.map((rsvp) => (
                <tbody key={rsvp.code}>
                  {rsvp.guests.map((guest) => (
                    <tr key={guest.id}>
                      <td>
                        <Link href={`/rsvp/${rsvp.code}`}>
                          {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                          <a>{rsvp.code}</a>
                        </Link>
                      </td>
                      <td>{guest.name || 'Guest'}</td>
                      <td>{guest.preferredName || guest.name || 'Guest'}</td>
                      <td>
                        <strong>
                          {guest.attending ? 'yes' : guest.attending === false ? 'no' : ''}
                        </strong>
                      </td>
                      <td>{guest.isPlusOne ? '+1' : guest.isPlusOne === false ? '' : ''}</td>
                      <td>{guest.dietaryRestrictions}</td>
                    </tr>
                  ))}
                </tbody>
              ))}
            </table>
          </details>
        </section>
      </main>
      <style jsx>{`
        header,
        main {
          max-width: 60rem;
          margin: auto;
          padding: 0 1rem;
        }

        table {
          width: 100%;
          border-collapse: collapse;
        }

        tbody tr:first-of-type td {
          padding-top: 1rem;
        }

        tbody tr:last-of-type td {
          border-bottom: thin solid black;
        }

        th {
          text-align: left;
        }

        thead {
          position: sticky;
          top: 0;
          background-color: white;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  const isAdmin = await getIsAdminByEmail(session.user.email)
  if (!isAdmin) {
    return { redirect: { destination: '/', permanent: false } }
  }
  const guests = await getAllGuests()
  const rsvps = buildRsvpGroups(guests)
  const serializableGuests = buildSerializableGuests(guests)
  const serializableRsvps = buildSerializableRsvpGroups(rsvps)
  serializableRsvps.sort((a, b) => b.guests[0].updatedAt - a.guests[0].updatedAt)

  return {
    props: {
      session,
      guests: serializableGuests,
      rsvps: serializableRsvps,
    },
  }
}

export default AdminPage
