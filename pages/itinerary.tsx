import Head from 'next/head'

const Itinerary = () => {
  return (
    <>
      <Head>
        <title>Shane &amp; Colleen&apos;s Wedding Itinerary</title>
      </Head>
      <main>
        <header>
          <h1>Wedding Itinerary</h1>
        </header>
        <div className="address-container">
          <div className="address">
            <p>The Watershed Institute</p>
            <p>31 Titus Mill Road</p>
            <p>Pennington, NJ 08534</p>
          </div>
          <div className="address">
            <p>Shane and Colleen&apos;s House</p>
            <p>106 Hopewell Rocky Hill Rd</p>
            <p>Hopewell, NJ 08525</p>
          </div>
        </div>
        <div className="schedule-container">
          <h2 className="left">Friday</h2>
          <div className="right"></div>
          <p className="time left">2:00pm</p>
          <div className="activity">
            <p>Rehearsal</p>
            <p>
              <em>The Watershed Institute</em>
            </p>
            <p>
              <em>Wedding Party only</em>
            </p>
          </div>
          <p className="time">5:00pm</p>
          <div className="activity">
            <p>Rehearsal Dinner</p>
            <p>
              <em>Shane and Colleen&apos;s House</em>
            </p>
            <p>
              <em>Wedding Party only</em>
            </p>
          </div>
          <h2>Saturday</h2>
          <div></div>
          <p className="time">4:00pm</p>
          <div className="activity">
            <p>Ceremony</p>
            <p>
              <em>The Watershed Institute</em>
            </p>
          </div>
          <p className="time">5:00pm</p>
          <div className="activity">
            <p>Cocktails and Reception</p>
            <p>
              <em>The Watershed Institute</em>
            </p>
          </div>
          <h2>Sunday</h2>
          <div></div>
          <p className="time">11:00am</p>
          <div className="activity">
            <p>Brunch with The Newlyweds</p>
            <p>
              <em>Shane and Colleen&apos;s House</em>
            </p>
          </div>
        </div>
      </main>
      <style jsx>{`
        main {
          margin: auto;
          max-width: 57rem;
          width: 100%;
          padding-bottom: 2rem;
        }
        main > * {
          width: 85%;
          margin-left: auto;
          margin-right: auto;
        }

        header {
          text-align: center;
        }
        h2 {
          border-right: 1px solid black;
          text-align: right;
          padding-right: 1rem;
          padding-bottom: 1rem;
        }
        .time {
          text-align: right;
          border-right: 1px solid black;
          padding-right: 1rem;
        }
        .activity {
          text-align: left;
          padding-bottom: 1rem;
        }
        .location {
          text-align: left;
          border-left: 1px solid black;
        }
        p,
        h2 {
          margin: 0;
        }
        .item-container {
          margin: 0;
        }
        .schedule-container {
          display: grid;
          grid-column-gap: 1rem;
          grid-template-columns: 45% 55%;
        }
        .address {
          text-align: center;
          display: inline-block;
          margin: 0 1rem;
          padding: 2rem 0 3rem 0;
        }
        .address-container {
          text-align: center;
          align-items: center;
        }
      `}</style>
    </>
  )
}
export default Itinerary
