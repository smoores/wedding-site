import Link from 'next/link'
import Head from 'next/head'
import { useEffect, useState } from 'react'

const backgroundUrlMap = {
  300: 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/300/Shane-247.jpg',
  500: 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/500/Shane-247.jpg',
  800: 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/800/Shane-247.jpg',
  1200: 'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/1200/Shane-247.jpg',
  fullsize:
    'https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/fullsize/Shane-247.jpg',
}

function hasScreenResolution(resolution: number) {
  return globalThis.matchMedia(`
          only screen and (-webkit-min-device-pixel-ratio: ${resolution}),
          only screen and (min--moz-device-pixel-ratio: ${resolution}),
          only screen and (-o-min-device-pixel-ratio: ${resolution}/1),
          only screen and (min-device-pixel-ratio: ${resolution}),
          only screen and (min-resolution: ${resolution * 96}dpi),
          only screen and (min-resolution: ${resolution}dppx)`)
}

function getBackgroundUrl() {
  const screenWidth = globalThis.screen.availWidth

  if (
    (!hasScreenResolution(2) && screenWidth >= 1200) ||
    (hasScreenResolution(2) && screenWidth >= 800)
  ) {
    return backgroundUrlMap.fullsize
  }
  if ((!hasScreenResolution(2) && screenWidth >= 800) || hasScreenResolution(2)) {
    return backgroundUrlMap[1200]
  }
  if (!hasScreenResolution(2) && screenWidth >= 500) {
    return backgroundUrlMap[800]
  }
  if (screenWidth >= 300) {
    return backgroundUrlMap[500]
  }
  return backgroundUrlMap[300]
}

const Home = () => {
  const [enhancedHeader, setEnhancedHeader] = useState(false)
  const [backgroundImage, setBackgroundImage] = useState('none')

  useEffect(() => {
    const backgroundUrl = getBackgroundUrl()
    const image = new Image()
    image.onload = () => setEnhancedHeader(true)
    image.src = backgroundUrl
    setBackgroundImage(`url('${backgroundUrl}')`)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  return (
    <>
      <Head>
        <title>Shane &amp; Colleen Get Married</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      <main>
        <header>
          <div className="enhanced"></div>
          <div className="panel">
            <h1>Shane &amp; Colleen</h1>
            <p className="subtitle">Are Getting Married!</p>
            <p className="description">
              <span>October 8, 2022</span> <span className="inline-divider">|</span>{' '}
              <span>The Watershed Institute, Hopewell, NJ</span>
            </p>
          </div>
        </header>
        <nav>
          <Link href="/itinerary">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Itinerary</a>
          </Link>
          <Link href="/gallery">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Wedding&nbsp;&amp;&nbsp;Engagement&nbsp;Photos</a>
          </Link>
          <Link href="/registry">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Registry</a>
          </Link>
        </nav>

        <section id="our-story">
          <h2>Our Story</h2>
          <p>
            Shane and Colleen met in the fall of 2013, at Cornell University Tae Kwon Do team
            practice. After a particularly long stretch of confusion and miscommunication, they
            finally started dating on May 17th, 2014, exactly two days before the end of the spring
            semester. They&apos;re real risk takers, those two.
          </p>
          <p>
            They spent that entire summer on opposite sides of the country, Colleen in Ithaca, and
            Shane in San Francisco. They texted and called each other on most days, and when
            they&apos;d finally had enough of being apart, Colleen flew to California and they spent
            the week together, fingers locked, inseparable.
          </p>
          <p>
            Colleen and Shane spent the rest of college as a team of two. They moved in together
            with the rest of the Tae Kwon Do team senior year, and when it came time for Shane to
            fly across the country again, this time to Seattle, Colleen came along, with no
            hesitation. After only a month of living in Seattle, our favorite couple had already
            adopted a puppy. You can see what we mean about the risk taking.
          </p>
          <p>
            Even though Colleen spent the next year in Germany, she and Shane grew closer every day,
            and when they moved to New York City together in 2017, they agreed to just quit it with
            all this long distance stuff. A brief three years, four gerbils, and another dog later,
            they finally got their acts together and got engaged.
          </p>
        </section>
        <section id="new-name">
          <h2>A New Family, and a New Name</h2>
          <p>
            Most often, when two people get married, they take the man&apos;s last name. But if
            you&apos;ve met Shane and Colleen, you probably know that &ldquo;everyone else is doing
            it&rdquo; isn&apos;t usually a very compelling argument for them. So, as they&apos;re
            prone to do, they started thinking.
          </p>
          <p>
            Family names help us tell our stories and forge our identities. They connect us to our
            ancestors, but also to our siblings. And Shane and Colleen&apos;s stories have a{' '}
            <em>lot</em> of family names. Moore, Higgins, Geller, Morrow, Kerner, Gibbons, Parker,
            the list is practically endless. These names have a power to them; they invoke feelings
            of love and solidarity.
          </p>
          <p>
            Some family names have sadness, too. Sometimes they remind us of those we love that are
            no longer with us. Sometimes names change, and sometimes they vanish altogether. Because
            women often take the names of their husbands, their family names can be lost. And
            sometimes names are lost for other reasons, like immigration and assimilation.
          </p>
          <p>Like we said, lots of thinking.</p>
          <p>
            Shane and Colleen spent a lot of time thinking about lost names. They thought about
            other naming traditions, too, like the Ashkenazi tradition of naming new family members
            after those that had recently passed away. They thought about Shane&apos;s grandmother,
            Elinor Geller, who recently passed away, and her mother, Ruth Friedman, who died when
            Shane was young. And they thought about the name Friedman, an Ashkenazi name that means
            &ldquo;one of peace&rdquo;.
          </p>
          <p>
            Soon, there will be a new family. It will join the Moores, the Higginses, Gellers and
            Morrows, Salvatos and Parkers. And it will have a new name.
          </p>
          <p>Friedman.</p>
        </section>
      </main>

      <style jsx>{`
        header {
          height: calc(100vh - 4rem);
          background-image: url("data:image/svg+xml;charset=utf-8,%3Csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' width='1500' height='2222' viewBox='0 0 1500 2222'%3E%3Cfilter id='blur' filterUnits='userSpaceOnUse' color-interpolation-filters='sRGB'%3E%3CfeGaussianBlur stdDeviation='20 20' edgeMode='duplicate' /%3E%3CfeComponentTransfer%3E%3CfeFuncA type='discrete' tableValues='1 1' /%3E%3C/feComponentTransfer%3E%3C/filter%3E%3Cimage filter='url(%23blur)' xlink:href='data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEBLAEsAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCAAoABsDAREAAhEBAxEB/8QAGQAAAgMBAAAAAAAAAAAAAAAACQUIBgcE/8QAKxAAAgIBBAEDAwMFAAAAAAAAAQIDBAUGEQcSACEIEyIxFAlBURUjNEJh/8QAGgEAAwEBAQEAAAAAAAAAAAAABQQGAwAHAf/EAC4RAAECBQMDAgYCAwAAAAAAAAECEQMhADEEQRIFcVFhE6EUgZEiMrHw8RXB0f/aAAwDAQACEQMRAD8A1P8ATna1wxi+WcRhcdhL+FwOu7IWPEZCJ2KsqSKkCSoqyJGqMABLGfpZTuBv5LZmfEgwjEBO3uGLF2drkWtNqK4+ImJEEM/l5k4az6HrWucSS6J5O5g5A0NNjMSo+Klk62IrQS0oFFuCWOzE9duhif5IFcow7bzHZnUhmGR14gT6hhboizeYskB2AlZxYgnrT2McgK9PeyEizA62mZ9joQKcYGWzxVynjVyM9q7Ph8XLh60jVUNixEJlmrLIC+z9IzKu+31Hc/SPXzTB52GiIooQ0VA7uJCR6tI/WtcjiVrQkLW8NXaVzMdHmPpQ/uPdMV6WHvpajxSzHNZVyJKg7gNkLDLvsCN9iPt6fx43H52IlbJiKFpAAh2mx612NwsNUMFUNJvMkg3l7UQf2C1bOguc/drjLmLuqaGboZpcbE/xWZ4XjnVYV22AYgKBsBuQASu3nZKoMDjVxIg2oSpyHb5S1nah43qywAXURIt7z6Up9x8uGyXuk0rLpjGSYXJap0vagsQXE+GzLbpzCzGiKd1eTpLMoYsRsCPQN2EmqNBy1kYYYQ9WYO0yCdLX60dgpXBD5M93l5aBh/qqByDqo3de4gvbGTp3krWvyWi6tOjEIhc9QO+7qGX9irgkgb+DvhzDQopUVNIzm85B2dhMe1G4UdCiBtCSZ+GlM3Z7eNagVitUZSmb8dmvSszLkru8hcR9h+TKR9IIAAGwGwHoPLpC4cNKUBIkBd+1TPpx4hK9xmT+6IxwbyWOPPdryFaz2ainx+e07Ujo90+aKV4ZoQ5BVmLO3yOOx6nqPQj1PkVzHKwIuHugpBClJmQS8i8pdr380ZhYMb1k7yZBUhJu37tbxVj9z2tYdU6k4pzhxePc6Zzsy14G2jZ1s15InUEb/wCvXqoI2ZP3J8GYXORPhjjyACTYNOVpmQ6SphfEI9QRZuSNX+v91lb60paf1Y0NPT/59id5xXYqXdS1iKyvZCSOwleT6unYBBuQD4t/kIq9oCmAMxq9r9LTFE0YCACSHl8mvbr1qCWR1BperqDMx5i7cbIrlbvylI+i/wCTJ1AUyAgBdv8An8ennoKOQdIKIQIYTqVPHhzvjEH5VO/RGkMJd/Uf1VpOV6uQpUtJ/kTSzp8KwSRRwFpWCkDaNC5+/r6k7eTWXgLiYARdW4eBYu3Ydq1xuXEOMIhkC/k+Hf3rj9wOsdEa04twVnRWaglk07qnT8lir8T17dKJ5GkiaeNiD0kBYiQbj023JG3iuNw8bFBEdP5AzDdCx8OPPenMnlPViMmRGn8/qlfuz5/057cNVST42xRt5fK4SZ5cgitFXhR2kjHyPIP7ZEoi7FQe4Ybt9JB+YXBxMghW+QaQHa4b+e9aL5xKE7Smb6mXV6FN7iNdx4PnvWlelbsCp/W7ksPxynoVeZ3BHr9j2389G+DQQHSNNKkk5a2/I0Zjj/hnVHCPvK1Ly9Qw9S/p/UWnmwk2Ee8peIEQKWSVizP2EZ3VlG/cjf028VUpGzaUi+n/ACnvgSYhU7+KofN3FlvlDGX6tPjLVGDr5HILbaSjNXhmiqwyiSvHESoCrGQgVSvQBSQpJ38Sg4DZCo6V/aQPt0dr/PuJnWtYigwSu4N9elJuV9N4zlDLY/NZzijTuTziVS8M+ZypyTSAP2DGGOUj77Md0ADNuB9vNkQfTdKVSPWnB6a/vUlyOlQf5E/TMz+vte5nNpS1mVyt2WztBgxJEhZySqNsN1U7gen2HhiHkqCQGFB14MNyyj9BX//Z' x='0' y='0' height='100%25' width='100%25'/%3E%3C/svg%3E%0A");
          background-size: cover;
          background-position: center 90%;
          letter-spacing: 0.5em;
          text-transform: lowercase;
        }

        .enhanced {
          opacity: ${enhancedHeader ? 1 : 0};
          transition: opacity 3s;
          padding: 5rem 0;
          width: 100%;
          height: 100%;
          background-image: ${backgroundImage};
          background-size: cover;
          background-position: center 90%;
        }

        .panel {
          position: absolute;
          top: 1rem;
          left: 50%;
          transform: translate(-50%);
          padding: 1.5rem 1.5rem 1rem;
          background-color: white;
          border: thin solid black;
          width: fit-content;
          margin: auto;
        }

        h1 {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
          font-weight: normal;
        }

        .subtitle {
          margin-top: 0;
          font-size: 3rem;
          line-height: 1.25;
        }

        h1,
        .subtitle,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        section {
          margin: auto;
          width: 85%;
          max-width: 900px;
        }

        h2 {
          font-size: 2rem;
        }

        @media (max-width: 760px) {
          h1 {
            font-size: 1.5rem;
          }

          .subtitle {
            font-size: 1.25rem;
          }

          .description {
            font-size: 1rem;
          }

          .enhanced {
            padding: 2rem 0;
          }

          .panel {
            width: 100%;
            border-right: none;
            border-left: none;
          }

          .panel > * {
            padding: 0 0.5rem;
          }

          .description span {
            display: block;
          }
          .description .inline-divider {
            display: none;
          }
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps() {
  return {
    props: {
      hideNav: true,
    },
  }
}

export default Home
