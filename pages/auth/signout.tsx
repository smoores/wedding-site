import { signOut } from 'next-auth/react'
import Button from '../../components/Button'

const SignOut = () => <Button onClick={() => signOut({ callbackUrl: '/' })}>Sign out</Button>

export default SignOut

export function getServerSideProps() {
  return {
    props: {
      hideNav: true,
    },
  }
}
