import Link from 'next/link'
import Button from '../../components/Button'
import { useRouter } from 'next/router'
import { getServerCsrfToken } from '../../server/getServerCsrfToken'
import { GetServerSideProps } from 'next'
import { setCookie } from '../../server/setCookie'

export default function SignIn({ csrfToken }) {
  const { query } = useRouter()
  return (
    <>
      <header>
        <h1>Sign in</h1>
      </header>
      <main>
        <p>
          Please sign in to access our registry or RSVP. If you haven&apos;t made an account,{' '}
          <Link href="/auth/signup">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>click here to sign up</a>
          </Link>
          .
        </p>
        {query.error === 'CredentialsSignin' && (
          <p className="error">
            The e-mail address or password you entered doesn&apos;t match what we have. Make sure
            that caps lock isn&apos;t on, or use a magic link if you can&apos;t remember your
            password!
          </p>
        )}
        {query.error && query.error !== 'CredentialsSignin' && (
          <p className="error">
            We couldn&apos;t sign you in, but we&apos;re not sure exactly why. Give Shane or Colleen
            a call when you have a chance!
          </p>
        )}
        <form method="post" action="/api/auth/callback/credentials">
          <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
          <label htmlFor="email">E-mail address</label>
          <input id="email" name="email" type="email" placeholder="example@gmail.com" />
          <label htmlFor="password">Password</label>
          <input id="password" name="password" type="password" />
          <Button type="submit">Sign in</Button>
        </form>
        <h2>Can&apos;t remember your password at the moment?</h2>
        <p>
          Enter your email address below and we&apos;ll email you a sign in link. No password
          required!
        </p>
        <form method="post" action="/api/auth/signin/email">
          <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
          <label htmlFor="email">Email address</label>
          <input type="email" id="email" name="email" />
          <Button type="submit">Sign in with a &ldquo;Magic Link&rdquo;</Button>
        </form>
      </main>
      <style jsx>{`
        header,
        main {
          margin: auto;
          width: 85%;
          max-width: 400px;
        }

        header {
          margin-top: 5rem;
        }

        p {
          margin-bottom: 2rem;
        }

        .error {
          padding: 0.5rem;
          background-color: #ffdddd;
        }

        label {
          display: block;
          margin-top: 0.5rem;
          margin-bottom: 0.25rem;
        }

        input[type='email'],
        input[type='password'] {
          display: block;
        }
      `}</style>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const csrfToken = await getServerCsrfToken(context)
  const callbackUrlHeader = context.query['callbackUrl']
  const callbackUrl = Array.isArray(callbackUrlHeader) ? callbackUrlHeader[0] : callbackUrlHeader
  if (callbackUrl) {
    const useSecureCookies = process.env.NEXTAUTH_URL?.startsWith('https://')
    const cookiePrefix = useSecureCookies ? '__Secure-' : ''
    setCookie(context.res, {
      name: `${cookiePrefix}next-auth.callback-url`,
      value: callbackUrl,
      options: {
        sameSite: 'lax',
        path: '/',
        secure: useSecureCookies,
      },
    })
  }

  return {
    props: { csrfToken },
  }
}
