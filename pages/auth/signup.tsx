import Link from 'next/link'
import { getCsrfToken } from 'next-auth/react'
import Button from '../../components/Button'

export default function SignUp({ csrfToken }) {
  return (
    <>
      <header>
        <h1>Sign up</h1>
      </header>
      <main>
        <p>
          Please make an account to access our registry. If you already have an account,{' '}
          <Link href="/auth/signin">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>click here to sign in</a>
          </Link>
          .
        </p>
        <form method="post" action="/api/auth/callback/credentials">
          <input name="csrfToken" type="hidden" defaultValue={csrfToken} />
          <label htmlFor="email">E-mail address</label>
          <input id="email" name="email" type="email" placeholder="example@gmail.com" />
          <label htmlFor="password">Password</label>
          <input id="password" name="password" type="password" />
          <Button type="submit">Sign up</Button>
        </form>
      </main>
      <style jsx>{`
        header,
        main {
          margin: auto;
          width: 85%;
          max-width: 400px;
        }

        header {
          margin-top: 5rem;
        }

        p {
          margin-bottom: 2rem;
        }

        label {
          display: block;
          margin-top: 0.5rem;
          margin-bottom: 0.25rem;
        }

        input {
          display: block;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const csrfToken = await getCsrfToken(context)
  return {
    props: { csrfToken },
  }
}
