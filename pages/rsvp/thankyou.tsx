import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { signIn, useSession } from 'next-auth/react'
import Button from '../../components/Button'
import ThankYouMessage from '../../components/rsvp/ThankYouMessage'
import { getSigninRedirect } from '../../server/getSigninRedirect'
import { options } from '../api/auth/[...nextauth]'

const RsvpThankYou = () => {
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
      </>
    )
  }

  return (
    <>
      <header>
        <h1>Thank you!</h1>
      </header>
      <main>
        <ThankYouMessage />
        <p>
          Need a hotel room? We have a block of rooms in Princeton.{' '}
          <a href="/accommodations">Check out our accommodations page.</a>
        </p>
      </main>
      <style jsx>{`
        header,
        main {
          max-width: 40rem;
          margin: auto;
          padding: 0 1rem;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  return {
    props: {
      session,
    },
  }
}

export default RsvpThankYou
