import { signIn, useSession } from 'next-auth/react'
import Head from 'next/head'
import RsvpForm from '../../components/rsvp/RsvpForm'
import HavingTroubleMessage from '../../components/rsvp/HavingTroubleMessage'
import { Guest } from '../../db/models/Guest'
import { getGuestsForRsvpCode, getIsAdminByEmail, getRsvpCodeByEmail } from '../../db/queries'
import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { options } from '../api/auth/[...nextauth]'
import { getSigninRedirect } from '../../server/getSigninRedirect'
import Button from '../../components/Button'

interface RsvpFormPageProps {
  guests: Guest[]
}

const RsvpFormPage = ({ guests }: RsvpFormPageProps) => {
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
      </>
    )
  }

  return (
    <>
      <Head>
        <title>RSVP to Shane &amp; Colleen&apos;s Wedding</title>
      </Head>
      <header>
        <h1>RSVP to Shane &amp; Colleen&apos;s Wedding</h1>
      </header>
      <main>
        <HavingTroubleMessage />
        <RsvpForm guests={guests} />
      </main>
      <style jsx>{`
        header,
        main {
          max-width: 40rem;
          margin: auto;
          padding: 0 1rem;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  const isAdmin = getIsAdminByEmail(session.user.email)
  const rsvpCode = await getRsvpCodeByEmail(session.user.email)
  if (!rsvpCode && !isAdmin) {
    return { redirect: { destination: '/rsvp/', permanent: false } }
  }
  if (rsvpCode !== context.params.code && !isAdmin) {
    context.res.writeHead(301, { Location: `/rsvp/${rsvpCode}` }).end()
    return { redirect: { destination: `/rsvp/${rsvpCode}`, permanent: false } }
  }
  const guests = await getGuestsForRsvpCode(context.params.code)

  return {
    props: {
      session,
      guests,
    },
  }
}

export default RsvpFormPage
