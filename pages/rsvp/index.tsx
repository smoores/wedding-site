import { GetServerSideProps } from 'next'
import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { signIn, useSession } from 'next-auth/react'
import Head from 'next/head'
import { useRouter } from 'next/router'
import Button from '../../components/Button'
import HavingTroubleMessage from '../../components/rsvp/HavingTroubleMessage'
import { getRsvpCodeByEmail } from '../../db/queries'
import { getSigninRedirect } from '../../server/getSigninRedirect'
import { options } from '../api/auth/[...nextauth]'

const Rsvp = () => {
  const router = useRouter()
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
        <style jsx>
          {`
            header,
            main {
              max-width: 40rem;
              margin: auto;
              padding: 0 1rem;
            }
          `}
        </style>
      </>
    )
  }

  return (
    <>
      <Head>
        <title>RSVP to Shane &amp; Colleen&apos;s Wedding</title>
      </Head>
      <header>
        <h1>Enter your RSVP code</h1>
      </header>
      <main>
        <p>
          Enter the code from your invitation below. It will be made up of four lowercase letters.
        </p>
        <HavingTroubleMessage />
        <form
          onSubmit={async (e) => {
            e.preventDefault()
            const formData = new FormData(e.target as HTMLFormElement)
            const rsvpCode = formData.get('rsvp-code')
            const body = {
              rsvpCode,
            }
            await fetch('/api/rsvp/code', {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(body),
            })
            router.push(`/rsvp/${rsvpCode}`)
          }}
        >
          <div>
            <label id="rsvp-code-label" htmlFor="rsvp-code">
              RSVP Code
            </label>
            <input id="rsvp-code" name="rsvp-code" type="text" />
          </div>
          <Button type="submit">Submit</Button>
        </form>
      </main>
      <style jsx>{`
        header,
        main {
          max-width: 40rem;
          margin: auto;
          padding: 0 1rem;
        }

        label {
          margin-right: 0.5rem;
        }
      `}</style>
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  const rsvpCode = await getRsvpCodeByEmail(session.user.email)
  if (rsvpCode) {
    return {
      redirect: {
        destination: `/rsvp/${rsvpCode}`,
        permanent: false,
      },
    }
  }

  return {
    props: {
      session,
    },
  }
}

export default Rsvp
