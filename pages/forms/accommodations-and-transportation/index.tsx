import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { signIn, useSession } from 'next-auth/react'
import Head from 'next/head'
import Button from '../../../components/Button'
import AccommAndTransportForm from '../../../components/forms/AccommAndTransportForm'
import { AccommAndTransportAnswers } from '../../../db/models/AccommAndTransport'
import { Guest } from '../../../db/models/Guest'
import {
  getAccommAndTransportAnswersByEmail,
  getGuestsForRsvpCode,
  getRsvpCodeByEmail,
} from '../../../db/queries'
import { getSigninRedirect } from '../../../server/getSigninRedirect'
import { options } from '../../api/auth/[...nextauth]'

interface AccommodationsAndTransportationFormProps {
  guests: Guest[]
  answers: AccommAndTransportAnswers | null
}

const AccommodationsAndTransportationForm = ({
  guests,
  answers,
}: AccommodationsAndTransportationFormProps) => {
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
      </>
    )
  }

  return (
    <>
      <Head>
        <title>Tell us about your plans!</title>
      </Head>
      <header>
        <h1>Tell us about your plans for our wedding!</h1>
      </header>
      <main>
        <AccommAndTransportForm guests={guests} answers={answers} />
      </main>
      <style jsx>{`
        header,
        main {
          max-width: 40rem;
          margin: auto;
          padding: 0 1rem;
        }
      `}</style>
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  const rsvpCode = await getRsvpCodeByEmail(session.user.email)
  const guests = rsvpCode ? await getGuestsForRsvpCode(rsvpCode) : null
  const answers = (await getAccommAndTransportAnswersByEmail(session.user.email)) ?? null

  return {
    props: {
      session,
      guests,
      answers,
    },
  }
}

export default AccommodationsAndTransportationForm
