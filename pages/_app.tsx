import Link from 'next/link'
import { SessionProvider } from 'next-auth/react'

export default function App({ Component, pageProps: { hideNav, ...pageProps } }) {
  return (
    <SessionProvider session={pageProps.session}>
      {!hideNav && (
        <nav>
          <Link href="/">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Home</a>
          </Link>{' '}
          <Link href="/itinerary">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Itinerary</a>
          </Link>{' '}
          <Link href="/gallery">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Wedding&nbsp;&amp;&nbsp;Engagement&nbsp;Photos</a>
          </Link>{' '}
          <Link href="/registry">
            {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
            <a>Registry</a>
          </Link>{' '}
        </nav>
      )}
      <Component {...pageProps} />
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: Baskerville;
          font-size: 18px;
          letter-spacing: 0.1em;
          line-height: 1.75;
        }

        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
          font-weight: normal;
          text-transform: lowercase;
        }

        * {
          box-sizing: border-box;
        }

        nav {
          padding: 0 1rem;
          max-width: 60rem;
          margin: 2rem auto 0;
          text-transform: lowercase;
        }

        nav > a {
          margin-right: 2rem;
          display: block;
        }

        nav > a:last-child {
          margin-right: 0;
        }

        input[type='text'],
        input[type='email'],
        input[type='password'] {
          display: inline-block;
          padding: 0.25rem 0.25rem 0;
          margin-bottom: 0.25rem;
          border-top: thin solid white;
          border-right: thin solid white;
          border-left: thin solid white;
          border-bottom: 2px solid currentcolor;
          font-size: 1rem;
          font-family: Baskerville;
          letter-spacing: 0.1rem;
        }

        input[type='text']:focus,
        input[type='email']:focus,
        input[type='password']:focus {
          outline: none;
          border-top: thin dashed currentcolor;
          border-right: thin dashed currentcolor;
          border-left: thin dashed currentcolor;
        }

        select {
          font-size: 0.9rem;
          font-family: Baskerville;
          letter-spacing: 0.1rem;
        }

        @media (min-width: 760px) {
          nav > a {
            display: inline;
          }
        }
      `}</style>
    </SessionProvider>
  )
}
