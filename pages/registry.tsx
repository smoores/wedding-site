import Head from 'next/head'
import { useSession, signIn } from 'next-auth/react'
import css from 'styled-jsx/css'
import RegistryItem from '../components/registry/RegistryItem'
import { RegistryItemModel, buildRegistryItems } from '../db/models/RegistryItem'
import { getRegistryItemsToUsers } from '../db/queries'
import { primaryBackground } from '../design/colors'
import Button from '../components/Button'
import { unstable_getServerSession as getServerSession } from 'next-auth/next'
import { options } from './api/auth/[...nextauth]'
import { getSigninRedirect } from '../server/getSigninRedirect'

interface RegistryProps {
  items: RegistryItemModel[]
}

const registryItemCss = css.resolve`
  * {
    margin: 2rem;
  }
`

const Registry = ({ items = [] }: RegistryProps) => {
  const { data: session, status } = useSession()

  if (status === 'loading') {
    return null
  }

  if (!session) {
    return (
      <>
        <header>
          <h1>Please sign in</h1>
        </header>
        <main>
          <Button type="button" onClick={() => signIn()}>
            Sign In
          </Button>
        </main>
      </>
    )
  }

  console.log(session)

  return (
    <>
      <Head>
        <title>Shane &amp; Colleen&apos;s Wedding Registry</title>
      </Head>
      <header>
        <h1>How to use this registry</h1>
      </header>
      <main>
        <section className="instructions">
          <p>
            After you buy something, be sure to click{' '}
            <span className="fake-button">I bought this</span> to let others know it&apos;s already
            been purchased!
          </p>
          <h3>Shipping address</h3>
          <address>
            Shane and Colleen
            <br />
            106 Hopewell Rocky Hill Rd
            <br />
            Hopewell, NJ 08525
            <br />
          </address>
        </section>
        <section>
          <ul>
            <li className={registryItemCss.className}>
              <div className="image-wrapper">
                <img
                  src="https://image.freepik.com/free-vector/summer-vacation-relax-cartoon_24640-45785.jpg"
                  alt=""
                />
              </div>
              <div>
                <p>Cash Fund</p>
                <p className="item-name">Honeymoon</p>
              </div>
              <p>
                Gift us on Venmo at <code>@Shane-Moore</code> or{' '}
                <a href="https://paypal.me/shanewmoore?locale.x=en_US">on PayPal</a>
              </p>
              <p>Or send a check to:</p>
              <div className="address">
                <p>Shane Moore</p>
                <p>106 Hopewell Rocky Hill Rd</p>
                <p>Hopewell, NJ 08525</p>
              </div>
            </li>
            <li className={registryItemCss.className}>
              <div className="image-wrapper">
                <img
                  src="https://f002.backblazeb2.com/file/shane-and-colleen-get-married-gallery/registry/DAEA7330-8692-47EB-9B7F-6D1157EECB28.jpeg"
                  alt="A colorful painting of an octopus"
                />
              </div>
              <div>
                <p>Nancy Stark</p>
                <p className="item-name">Any of her wonderful animal paintings</p>
                <p>
                  The best way to reach out to Nancy is on{' '}
                  <a href="https://www.instagram.com/nancystark.arts" rel="noopener noreferrer">
                    her Instagram.
                  </a>
                </p>
              </div>
            </li>
            {items.map((item) => (
              <RegistryItem className={registryItemCss.className} key={item.id} item={item} />
            ))}
          </ul>
        </section>
      </main>
      <style jsx>{`
        main,
        header {
          margin: auto;
          max-width: 57rem;
          width: 100%;
        }

        main {
          letter-spacing: normal;
        }

        header {
          padding: 0 1rem;
        }

        .instructions {
          padding: 0 1rem;
        }

        .fake-button {
          font-family: Helvetica Neue;
          border: none;
          border-radius: 0.25rem;
          padding: 0.4rem 0.6rem;
          margin-top: 0.4rem;
          margin-bottom: 0.4rem;
          font-size: 0.8rem;
          color: white;
          background: ${primaryBackground};
          white-space: nowrap;
        }

        ul {
          display: flex;
          flex-wrap: wrap;
          justify-content: center;
          list-style: none;
          padding: 0;
          text-align: center;
        }

        li {
          display: flex;
          flex-direction: column;
          flex-shrink: 0;
          flex-basis: 15rem;
          text-align: left;
          position: relative;
        }

        li p {
          font-size: 0.8rem;
          margin: 0.25rem;
        }

        li p.item-name {
          font-size: 1rem;
        }

        .image-wrapper {
          display: flex;
          height: 15rem;
          margin-bottom: 0.5rem;
        }

        .image-wrapper img {
          display: block;
          border-radius: 0.25rem;
          max-width: 100%;
          max-height: 15rem;
          margin: auto;
        }

        .address {
          margin-left: 1rem;
          color: ;
        }
      `}</style>
      {registryItemCss.styles}
    </>
  )
}

export async function getServerSideProps(context) {
  const session = await getServerSession(context.req, context.res, options)
  if (!session) {
    const redirect = await getSigninRedirect(context)
    return { redirect }
  }
  const results = await getRegistryItemsToUsers()
  console.log(session)
  return {
    props: {
      session,
      ...(session && session.user && { items: buildRegistryItems(results, session.user) }),
    },
  }
}

export default Registry
