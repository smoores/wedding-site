import { NextApiRequest, NextApiResponse } from 'next'
import { Guest } from '../../../db/models/Guest'
import { updateGuests } from '../../../db/queries'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'PUT') {
    return res.status(405).end()
  }
  const guests: Array<Partial<Guest>> = req.body.guests
  await updateGuests(guests)
  return res.status(204).end()
}
