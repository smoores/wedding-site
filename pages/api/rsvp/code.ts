import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/react'
import { updateRsvpCodeByEmail } from '../../../db/queries'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'PUT') {
    return res.status(405).end()
  }
  const { rsvpCode } = req.body
  const session = await getSession({ req })
  await updateRsvpCodeByEmail(rsvpCode.toLowerCase(), session.user.email)
  res.status(204).end()
}
