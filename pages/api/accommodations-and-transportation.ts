import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/react'
import { getUserIdByEmail, setAccommAndTransportAnswers } from '../../db/queries'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'PUT') {
    return res.status(405).end()
  }
  const session = await getSession({ req })
  const { accommodations, shuttle, brunch, freeform, answeringFor } = req.body
  const userId = await getUserIdByEmail(session.user.email)
  await setAccommAndTransportAnswers(
    userId,
    accommodations,
    shuttle,
    brunch,
    freeform,
    answeringFor
  )
  res.status(204).end()
}
