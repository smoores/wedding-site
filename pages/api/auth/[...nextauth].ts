import NextAuth, { NextAuthOptions } from 'next-auth'
import CredentialsProvider from 'next-auth/providers/credentials'
import EmailProvider from 'next-auth/providers/email'
import { NextApiRequest, NextApiResponse } from 'next'
import { hash, verify } from 'argon2'
import { blockList } from '../../../blockList'
import { createUser, getUserByEmail, updateUserPasswordHash } from '../../../db/queries'
import { PostgresAdapter } from '../../../db/adapter'
import { Adapter } from 'next-auth/adapters'

export const options: NextAuthOptions = {
  secret: process.env.NEXTAUTH_SECRET,
  session: {
    strategy: 'jwt',
  },
  pages: {
    signIn: '/auth/signin',
    signOut: '/auth/signout',
  },
  providers: [
    CredentialsProvider({
      name: 'Password',
      credentials: {
        email: { label: 'Email address', type: 'email', placeholder: 'example@gmail.com' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials: { email: string; password: string }) {
        const user = await getUserByEmail(credentials.email)
        if (!user) {
          const passwordHash = await hash(credentials.password, {
            secret: Buffer.from(process.env.NEXTAUTH_SECRET),
          })
          await createUser(credentials.email, passwordHash)
          const { passwordHash: _, ...userResult } = await getUserByEmail(credentials.email)
          return userResult
        }
        if (user.passwordHash === null) {
          const passwordHash = await hash(credentials.password, {
            secret: Buffer.from(process.env.NEXTAUTH_SECRET),
          })
          await updateUserPasswordHash(user.id, passwordHash)
          const { passwordHash: _, ...userResult } = user
          return userResult
        }
        try {
          const valid = await verify(user.passwordHash, credentials.password, {
            secret: Buffer.from(process.env.NEXTAUTH_SECRET),
          })
          if (valid) {
            const { passwordHash: _, ...userResult } = user
            return userResult
          }
        } catch (e) {
          console.error(e)
        }
        return null
      },
    }),
    EmailProvider({
      server: {
        host: process.env.EMAIL_SERVER_HOST,
        port: parseInt(process.env.EMAIL_SERVER_PORT, 10),
        auth: {
          user: process.env.EMAIL_SERVER_USER,
          pass: process.env.EMAIL_SERVER_PASSWORD,
        },
        ignoreTLS: true,
      },
      from: process.env.EMAIL_FROM,
    }),
  ],
  adapter: PostgresAdapter as Adapter,
  callbacks: {
    // Disable signing in for anyone who's not on the guest list
    signIn: async ({ profile }) => !blockList.includes(profile?.email),
    session: async ({ session, user }) =>
      blockList.includes(user?.email) ? { expires: Date.now().toLocaleString() } : session,
  },
}

export default (req: NextApiRequest, res: NextApiResponse) => NextAuth(req, res, options)
