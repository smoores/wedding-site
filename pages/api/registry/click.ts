import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/react'
import { getUserIdByEmail, incrementRegistryItemNumberOfClicks } from '../../../db/queries'

export default async (req: NextApiRequest, res: NextApiResponse) => {
  if (req.method !== 'PUT') {
    return res.status(405).end()
  }
  const { registryItemId } = req.body
  const session = await getSession({ req })
  const userId = await getUserIdByEmail(session.user.email)
  await incrementRegistryItemNumberOfClicks(registryItemId, userId)
  res.status(204).end()
}
