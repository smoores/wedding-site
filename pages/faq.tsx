const FaqPage = () => {
  return (
    <>
      <main>
        <header>
          <h1>Frequently Asked Questions</h1>
        </header>
        <div>
          <h2>Will the wedding be inside or outside?</h2>
          <p>The ceremony will be outside, but the reception will be indoors.</p>
          <h2>What&apos;s the dress code?</h2>
          <p>
            Dress code is semi-formal. The wedding ceremony will be outside on the grass, so be sure
            to plan footwear accordingly.
          </p>
          <h2>What is the wedding weekend going to look like?</h2>
          <p>
            We have a rough schedule for friday through sunday on our{' '}
            <a href="/itinerary">itinerary page</a> including the location for each activity.
          </p>
          <h2>Where should I stay while I&apos;m in town?</h2>
          <p>
            We have a discounted rate for our guests at the Hyatt Regency in Princeton, you can find
            more information including how to book with our rate on{' '}
            <a href="/accommodations">the accommodations page.</a>
          </p>
          <h2>How will I get to the wedding venue?</h2>
          <p>
            There will be a transportation service between the Hyatt Regency and the venue, or you
            may drive yourself. The venue address is on <a href="/itinerary">the itinerary page.</a>
          </p>
          <h2>Where do I find your registry?</h2>
          <p>
            You will find our <a href="/registry">registry page here.</a>
          </p>
          <h2>I need to update or change my rsvp information!</h2>
          <p>
            No problem! You can visit <a href="/rsvp">the rsvp page</a> again and make any necessary
            changes
          </p>
          <h2>I have a question that wasn&apos;t answered here!</h2>
          <p>
            Please let us know! You can email us at{' '}
            <a href="mailto:shaneandcolleengetmarried@mailbeaver.net">
              shaneandcolleengetmarried@mailbeaver.net
            </a>{' '}
            and we will answer you as soon as we can (and possibly add the answer here).
          </p>
        </div>
      </main>
      <style jsx>
        {`
          main {
            max-width: 48rem;
            margin: auto;
          }

          main > * {
            width: 95%;
            margin-left: auto;
            margin-right: auto;
          }

          div {
            display: grid;
            grid-template-columns: [h2] 40% [p] 60%;
          }

          h2 {
            width: 70%;
            margin-left: auto;
            margin-right: auto;
            font-size: 1rem;
          }

          header {
            text-align: center;
          }
        `}
      </style>
    </>
  )
}

export default FaqPage
