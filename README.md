This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/zeit/next.js/tree/canary/packages/create-next-app).

## Getting Started

You'll need a `.env.local` file with settings for the following environment variables:

```
EMAIL_SERVER_USER
EMAIL_SERVER_PASSWORD
EMAIL_SERVER_HOST
EMAIL_SERVER_PORT
EMAIL_FROM
DB_HOST
DB_PORT
DB_USERNAME
DB_PASSWORD
DB_DATABASE
AUTH_SECRET
BLOCK_LIST
```

### Development

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can make changes to any of the files and the dev server will hot reload automatically, and your changes will appear in the browser.

## Production Build

Run `yarn build` to generate a production build, and then `yarn start` to serve the site.

## Scripts

There's a script for importing new registry items in `scripts/importRegistryItems.ts`. You can call it with `yarn run import`. Make sure to run `yarn build` after making any changes to the script or its dependencies.

The JSON file passed to `yarn run import` should be an array of objects, where each object implements the `RegistryItemDBRecord` interface (defined in `db/models/registry/RegistryItemModel.ts`)

```ts
interface RegistryItemDbRecord {
  id: number
  image_url?: string
  store_url?: string
  manufacturer_name?: string
  name: string
  price?: string
  requested_number: number
  quantity: number
  email: string
}
```
